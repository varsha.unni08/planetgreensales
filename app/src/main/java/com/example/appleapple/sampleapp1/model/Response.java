package com.example.appleapple.sampleapp1.model;

public class Response{
	private String vlBalance;
	private String cdCustomer;
	private String dsCustomer;

	public void setVlBalance(String vlBalance){
		this.vlBalance = vlBalance;
	}

	public String getVlBalance(){
		return vlBalance;
	}

	public void setCdCustomer(String cdCustomer){
		this.cdCustomer = cdCustomer;
	}

	public String getCdCustomer(){
		return cdCustomer;
	}

	public void setDsCustomer(String dsCustomer){
		this.dsCustomer = dsCustomer;
	}

	public String getDsCustomer(){
		return dsCustomer;
	}
}
