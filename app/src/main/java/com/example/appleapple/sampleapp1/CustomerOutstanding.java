package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.adapters.RecyclerAdapterBranch;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Customer;
import com.example.appleapple.sampleapp1.model.Order;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CustomerOutstanding extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {

    ArrayList data = new ArrayList<Order>();

    String Token;
    private AutoCompleteTextView customer;
    private Customer selection1;
    private ArrayAdapter<Customer> custAdapter;

    ImageView imgback;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cancel_order);
        getSupportActionBar().hide();
        customer = (AutoCompleteTextView) findViewById(R.id.customer);
        imgback=findViewById(R.id.imgback);
        getToken();
        if(NetworkChangeReceiver.isConnected()){

        fillcustomer();}else{
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }


        Button clear=(Button)findViewById(R.id.BtnSearch);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customer.setText("");
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }

    public void fillcustomer() {
        final ArrayList<Customer> custArray = new ArrayList<>();
        // custArray.add("Anu");
        //  custArray.add("sunil");
        String Cr_Url = Constants.base_url+"get_customer.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        Customer customer = new Customer();
                        customer.setCdCustomer(brObject.getString("cd_customer"));
                        customer.setDsCustomer(brObject.getString("ds_customer"));
                        customer.setDsPhoneNo(brObject.getString("ds_phone_no"));
                        customer.setDsEmail(brObject.getString("ds_email"));
                        customer.setDsHouseName(brObject.getString("ds_house_name"));
                        customer.setDsStreetName(brObject.getString("ds_street_name"));
                        customer.setDsLocation(brObject.getString("ds_location"));
                        customer.setDsLocation(brObject.getString("ds_tin_number"));
                        Log.d("Customer", brObject.getString("ds_customer"));
                        custArray.add(customer);

                    }
                    custAdapter = new ArrayAdapter<Customer>(CustomerOutstanding.this, android.R.layout.simple_list_item_1, custArray);
                    customer.setAdapter(custAdapter);
                    customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                            selection1 = (Customer) parent.getItemAtPosition(position);
                            FillOrderData(selection1);
                            //TODO Do something with the selected text
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtres = (TextView) findViewById(R.id.Res);
                txtres.setText(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
//********************************************************

    }

    public void FillOrderData(Customer c) {
        final ArrayList<Branch> branchs = new ArrayList<>();
        String Cr_Url = Constants.base_url+"customer_outstanding.php?cd_customer=" + c.getCdCustomer();
        Log.v("TAG", "-->" + Cr_Url);
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONObject jsonObject = new JSONObject(response);
                    TextView tv = (TextView) findViewById(R.id.total);
                    tv.setText("Total balance : RS " + jsonObject.getString("total_balance"));
                    JSONArray res = jsonObject.getJSONArray("details");
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);

                        String stk, name, id;
                        name = brObject.getString("ds_branch");
                        id = brObject.getString("cd_branch");
                        stk = brObject.getString("vl_balance");
                        System.out.println(name + stk);
                        branchs.add(new Branch(id, name, stk));

                    }
                    RecyclerView orderlist = (RecyclerView) findViewById(R.id.Orderslist);
                    RecyclerAdapterBranch adapter = new RecyclerAdapterBranch(branchs, CustomerOutstanding.this, selection1);
                    orderlist.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"No data present",Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //************************************************
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }
}
