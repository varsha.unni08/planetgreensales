package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.OrderItem;

import java.util.ArrayList;

public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.OrderViewHolder> implements Filterable {


    private final Context context;
    ArrayList<OrderItem> morderlist;
    ArrayList<OrderItem> morderlistfull;

    public OnItemClickListener mlistnere;

    public interface OnItemClickListener {
        void OnItemClick(int position);


    }

    public void SetOnItemClickListener(OnItemClickListener listener) {
        mlistnere = listener;

    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        public TextView T_oid, T_cust;
        ImageView edit, delete;

        public OrderViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            T_oid = (TextView) itemView.findViewById(R.id.Txtvw_oid);
            T_cust = (TextView) itemView.findViewById(R.id.Txtvw_cust);
            edit = (ImageView) itemView.findViewById(R.id.btn_edit);
            delete = (ImageView) itemView.findViewById(R.id.can_order);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnItemClick(position);
                        }
                    }

                }
            });
        }
    }

    public OrderRecyclerAdapter(ArrayList<OrderItem> orderlist, Context context) {
        morderlist = orderlist;
        this.context = context;
        morderlistfull = new ArrayList<>(orderlist);

    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_orderlayout, parent, false);
        OrderViewHolder ov = new OrderViewHolder(v, mlistnere);
        return ov;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder orderViewHolder, final int pos) {
        final OrderItem currentItem = morderlist.get(pos);
        orderViewHolder.T_oid.setText("Sales Order Id  :" + currentItem.getCdSalesOrder());
        orderViewHolder.T_cust.setText("Customer Name  " + currentItem.getDsCustomer());


    }


    @Override
    public int getItemCount() {
        return morderlist.size();
    }

    @Override
    public Filter getFilter() {
        return orderfilter;
    }

    private Filter orderfilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            ArrayList<OrderItem> filteredOrder = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0) {
                filteredOrder.addAll(morderlistfull);
            } else {
                String searchpattern = charSequence.toString().toLowerCase();
                for (OrderItem item : morderlistfull) {
                    if (item.getDsCustomer().toLowerCase().startsWith(searchpattern)) {
                        filteredOrder.add(item);
                    }
                }

            }
            FilterResults results = new FilterResults();
            results.values = filteredOrder;
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            morderlist.clear();
            morderlist.addAll((ArrayList) filterResults.values);
            notifyDataSetChanged();

        }
    };
}
