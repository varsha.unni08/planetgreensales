package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Customer;
import com.example.appleapple.sampleapp1.model.State;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewCustomer extends AppCompatActivity {

    AutoCompleteTextView txtname;
    EditText txtphno;
    EditText txtemail;
    EditText txthname;
    EditText txtstreet;
    EditText txtloc;
    Spinner statespinner;
    EditText tinno;

    ArrayList<String> state = new ArrayList<String>();

    ImageView imgback;


    String Token;
    private ArrayAdapter<Customer> custAdapter;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        getSupportActionBar().hide();
        txtname = (AutoCompleteTextView) findViewById(R.id.cname);
        txtname.requestFocus();
        txtphno = (EditText) findViewById(R.id.phno);
        txtemail = (EditText) findViewById(R.id.email);
        txthname = (EditText) findViewById(R.id.hname);
        txtstreet = (EditText) findViewById(R.id.street);
        txtloc = (EditText) findViewById(R.id.loc);
        statespinner = (Spinner) findViewById(R.id.state);
        tinno = (EditText) findViewById(R.id.tin_no);

        imgback=findViewById(R.id.imgback);
        getToken();
        Fillstate();fillcustomer();
        Button btnAddCus = (Button) findViewById(R.id.Btn_add);
        btnAddCus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();


            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }

    private void Fillstate() {
        final ArrayList<State> states = new ArrayList<>();
        final Spinner staespinner = (Spinner) findViewById(R.id.state);
        String Cr_Url = Constants.base_url+"view_states.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        System.out.println(brObject.getString("cd_state"));
                        states.add(new State(brObject.getString("cd_state"), brObject.getString("ds_state")));

                    }
                    ArrayAdapter<State> adapter = new ArrayAdapter<State>(NewCustomer.this, android.R.layout.simple_list_item_1, states);
                    staespinner.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //************************************************
    }

    private void validate() {
        boolean nameflag = false;
        boolean phnoflag = false;
        boolean emailflag = false;
        boolean hnameflag = false;
        boolean streetflag = false;
        boolean locflag = false;
        if (TextUtils.isEmpty(txtname.getText().toString())) {
            nameflag = false;
            Toast.makeText(this, "Enter customer name", Toast.LENGTH_LONG).show();
            txtname.requestFocus();

        } else {
            nameflag = true;
        }
        if (txtphno.getText().toString().matches("^[6-9]\\d{9}$")) {
            phnoflag = true;

        } else {
            phnoflag = false;
            Toast.makeText(this, "Enter valid Phone number", Toast.LENGTH_LONG).show();
            txtphno.requestFocus();


        }
        if (Patterns.EMAIL_ADDRESS.matcher(txtemail.getText().toString()).matches()) {
            emailflag = true;

        } else {
            emailflag = false;
            Toast.makeText(this, "Enter valid Email id", Toast.LENGTH_LONG).show();
            txtemail.requestFocus();

        }
        if (TextUtils.isEmpty(txthname.getText().toString())) {
            hnameflag = false;
            Toast.makeText(this, "Enter valid Address", Toast.LENGTH_LONG).show();
            txthname.requestFocus();
        } else {
            hnameflag = true;
        }
        if (TextUtils.isEmpty(txtstreet.getText().toString())) {
            streetflag = false;
            Toast.makeText(this, "Enter valid Address", Toast.LENGTH_LONG).show();
            txtstreet.requestFocus();
        } else {
            streetflag = true;
        }
        if (TextUtils.isEmpty(txtloc.getText().toString())) {
            locflag = false;
            Toast.makeText(this, "Enter valid location", Toast.LENGTH_LONG).show();
            txtloc.requestFocus();
        } else {
            locflag = true;
        }
        if ((nameflag == true) && (phnoflag == true) && (emailflag == true) && (hnameflag == true) && (streetflag == true) && (locflag == true)) {
            Addcustomer();
        } else {
            Toast.makeText(this, "Enter valid data", Toast.LENGTH_LONG).show();

        }

    }

    private void Addcustomer() {
        String url = Constants.base_url+"add_customer.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray res = null;
                try {
                    res = new JSONArray(response);
                    JSONObject brObject = res.getJSONObject(0);
                    if (brObject.getInt("status") == 0) {
                        Cleardata();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;

            }

            ;

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ds_customer", txtname.getText().toString());
                params.put("ds_phone_no", txtphno.getText().toString());
                params.put("ds_email", txtemail.getText().toString());
                params.put("ds_house_name", txthname.getText().toString());
                params.put("ds_street_name", txtstreet.getText().toString());
                params.put("ds_location", txtloc.getText().toString());
                params.put("cd_state", ((State) statespinner.getSelectedItem()).getCdState());
                params.put("ds_tin_number", tinno.getText().toString());
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

    public void Cleardata() {
        txtname.setText("");
        txtphno.setText("");
        txtemail.setText("");
        txthname.setText("");
        txtstreet.setText("");
        txtloc.setText("");
        tinno.setText("");
    }

    public void fillcustomer() {
        final ArrayList<Customer> custArray = new ArrayList<>();
        // custArray.add("Anu");
        //  custArray.add("sunil");
        String Cr_Url = Constants.base_url+"get_customer.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        Customer customer = new Customer();
                        customer.setCdCustomer(brObject.getString("cd_customer"));
                        customer.setDsCustomer(brObject.getString("ds_customer"));
                        customer.setDsPhoneNo(brObject.getString("ds_phone_no"));
                        customer.setDsEmail(brObject.getString("ds_email"));
                        customer.setDsHouseName(brObject.getString("ds_house_name"));
                        customer.setDsStreetName(brObject.getString("ds_street_name"));
                        customer.setDsLocation(brObject.getString("ds_location"));
                        customer.setDsLocation(brObject.getString("ds_tin_number"));
                        Log.d("Customer", brObject.getString("ds_customer"));
                        custArray.add(customer);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtres = (TextView) findViewById(R.id.Res);
                txtres.setText(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
//********************************************************
        custAdapter = new ArrayAdapter<Customer>(this, android.R.layout.simple_list_item_1, custArray);
        txtname.setAdapter(custAdapter);
        txtname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                //TODO Do something with the selected text
            }
        });

    }
}
