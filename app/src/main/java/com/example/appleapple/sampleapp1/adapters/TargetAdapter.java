package com.example.appleapple.sampleapp1.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.model.Target;

import java.util.List;

public class TargetAdapter  extends RecyclerView.Adapter<TargetAdapter.TargetHolder> {


    Context context;
    List<Target>targets;

    public TargetAdapter(Context context, List<Target> targets) {
        this.context = context;
        this.targets = targets;
    }

    public class TargetHolder extends RecyclerView.ViewHolder
    {

        TextView txtPattern;

        public TargetHolder(@NonNull View itemView) {
            super(itemView);

            txtPattern=itemView.findViewById(R.id.txtPattern);
        }
    }

    @NonNull
    @Override
    public TargetHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_stockadapter,viewGroup,false);




        return new TargetHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TargetHolder targetHolder, int i) {


        targetHolder.txtPattern.setText("Acheivement : "+targets.get(i).getVlAchievementInRupees()+"\nTarget amount : "+targets.get(i).getVlTargetInRupees()+"\n"+targets.get(i).getDsTargetInSqt());


    }

    @Override
    public int getItemCount() {
        return targets.size();
    }
}
