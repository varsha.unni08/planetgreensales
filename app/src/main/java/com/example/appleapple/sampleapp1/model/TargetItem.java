package com.example.appleapple.sampleapp1.model;

import java.util.HashMap;
import java.util.Map;

public class TargetItem {

    private String cdSalesOrder;
    private String cdBranch;
    private String dtDelivery;
    private String cdCustomer;
    private String dsCustomer;
    private String vlTotal;
    private String dsDeliveryAddress;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public TargetItem() {
    }

    /**
     *
     * @param dsCustomer
     * @param cdSalesOrder
     * @param dsDeliveryAddress
     * @param dtDelivery
     * @param cdCustomer
     * @param cdBranch
     * @param vlTotal
     */
    public TargetItem(String cdSalesOrder, String cdBranch, String dtDelivery, String cdCustomer, String dsCustomer, String vlTotal, String dsDeliveryAddress) {
        super();
        this.cdSalesOrder = cdSalesOrder;
        this.cdBranch = cdBranch;
        this.dtDelivery = dtDelivery;
        this.cdCustomer = cdCustomer;
        this.dsCustomer = dsCustomer;
        this.vlTotal = vlTotal;
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public String getCdSalesOrder() {
        return cdSalesOrder;
    }

    public void setCdSalesOrder(String cdSalesOrder) {
        this.cdSalesOrder = cdSalesOrder;
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getDtDelivery() {
        return dtDelivery;
    }

    public void setDtDelivery(String dtDelivery) {
        this.dtDelivery = dtDelivery;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getVlTotal() {
        return vlTotal;
    }

    public void setVlTotal(String vlTotal) {
        this.vlTotal = vlTotal;
    }

    public String getDsDeliveryAddress() {
        return dsDeliveryAddress;
    }

    public void setDsDeliveryAddress(String dsDeliveryAddress) {
        this.dsDeliveryAddress = dsDeliveryAddress;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}