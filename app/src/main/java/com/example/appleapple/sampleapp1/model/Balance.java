package com.example.appleapple.sampleapp1.model;

import java.util.List;

public class Balance{
	private int totalBalance;
	private List<DetailsItem> details;

	public void setTotalBalance(int totalBalance){
		this.totalBalance = totalBalance;
	}

	public int getTotalBalance(){
		return totalBalance;
	}

	public void setDetails(List<DetailsItem> details){
		this.details = details;
	}

	public List<DetailsItem> getDetails(){
		return details;
	}

	@Override
 	public String toString(){
		return 
			"Balance{" + 
			"total_balance = '" + totalBalance + '\'' + 
			",details = '" + details + '\'' + 
			"}";
		}
}