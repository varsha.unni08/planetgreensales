package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.OrderInfo;

import java.util.ArrayList;

public class BranchAdpaterNew extends ArrayAdapter <OrderInfo>{

    Context mcontext ;
    int mresource;

    public BranchAdpaterNew(@NonNull Context context, int resource, @NonNull ArrayList<OrderInfo> objects) {
        super(context, resource, objects);
        mcontext=context;
        mresource=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {




        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView prdname=(TextView) convertView.findViewById(R.id.Txtvw_Pname);
        TextView prdQty=(TextView) convertView.findViewById(R.id.Txtvw_stock);
        TextView date=(TextView) convertView.findViewById(R.id.date);
        TextView balamount=(TextView) convertView.findViewById(R.id.balamount);
        TextView paidamount=(TextView) convertView.findViewById(R.id.paidamount);
        String name=getItem(position).getDsSalesInvoice();
        String stock=getItem(position).getCdSalesOrder();

        prdname.setText("Invoice id : "   +String.valueOf(name));
        prdQty.setText("Order Id : "+String.valueOf(stock));
        date.setText("Date : "+getItem(position).getDtProductSale());
        balamount.setText("Balance amount  : Rs  "+getItem(position).getVlBalance());
        paidamount.setText("Paid amount  : Rs "+getItem(position).getVlPaidAmount());
        return  convertView;


    }
}
