package com.example.appleapple.sampleapp1.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.BranchAdpater;
import com.example.appleapple.sampleapp1.BranchAdpaterNew;
import com.example.appleapple.sampleapp1.CustomerAdpater;
import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Customer;
import com.example.appleapple.sampleapp1.model.OrderInfo;
import com.example.appleapple.sampleapp1.model.Stock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sooraj Soman on 5/2/2019
 */
public class RecyclerAdapterBranchOutstanding extends RecyclerView.Adapter<RecyclerAdapterBranchOutstanding.BranchViewHolder> {
    private final Context context;
    private List<Branch> items;


    public RecyclerAdapterBranchOutstanding(List<Branch> items, Context context) {
        this.items = items;
        this.context = context;

    }

    @Override
    public BranchViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stock_list, parent, false);
        return new BranchViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BranchViewHolder holder, final int position) {
        final Branch item = items.get(position);
        holder.set(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postdataAsync(items.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class BranchViewHolder extends RecyclerView.ViewHolder {
        TextView prdname, Qty;

        public BranchViewHolder(View itemView) {
            super(itemView);
            prdname = (TextView) itemView.findViewById(R.id.Txtvw_Pname);
            Qty = (TextView) itemView.findViewById(R.id.Txtvw_stock);
        }

        public void set(Branch item) {
            prdname.setText("Branch  : " + String.valueOf(item.getBname()));
            Qty.setText("Total balance : " + String.valueOf(item.getItemQty()));
            //UI setting code
        }
    }


    private void postdataAsync(Branch branch, Integer pos) {
        final ArrayList<com.example.appleapple.sampleapp1.model.Response> orderInfos = new ArrayList<>();
        SharedPreferences sharedpref = context.getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        final String Token = "Bearer " + sharedpref.getString("token", "not null");
        String stockurl = Constants.base_url+"branch_outstanding.php?cd_branch=" + branch.getBcode();
        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name, id;
                        name = brObject.getString("ds_customer");
                        id = brObject.getString("cd_customer");
                        stk = brObject.getString("vl_balance");
                        System.out.println(name + stk);
                        com.example.appleapple.sampleapp1.model.Response response1=new com.example.appleapple.sampleapp1.model.Response();
                        response1.setCdCustomer(id);
                        response1.setDsCustomer(name);
                        response1.setVlBalance(stk);
                        orderInfos.add(response1);


                    }
                    showDialog(orderInfos);

                } catch (JSONException e) {
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(context);
        reque.add(sr);
    }

    private void showDialog(ArrayList<com.example.appleapple.sampleapp1.model.Response> stocks) {
        final AlertDialog.Builder mbuilder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View mview = layoutInflater.inflate(R.layout.list_stockview, null);
        mbuilder.setView(mview);
        ListView listView = (ListView) mview.findViewById(R.id.rec_view);
        mbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        CustomerAdpater adapter = new CustomerAdpater(context, R.layout.stock_list, stocks);
        listView.setAdapter(adapter);
        final AlertDialog dialog = mbuilder.create();
        dialog.show();
        dialog.setTitle("Branch View");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

    }


}