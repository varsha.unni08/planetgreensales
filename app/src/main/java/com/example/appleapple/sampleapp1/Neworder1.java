package com.example.appleapple.sampleapp1;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Customer;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Neworder1 extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, NetworkChangeReceiver.ConnectivityReceiverListener {


    final String Br_Url = Constants.base_url+"all_branches.php";

    String Token;
    ArrayList<Branch> spinnerArray_br;
    Spinner branch;
    AutoCompleteTextView customer;
    EditText del_address;
    TextView Del_date;
    ArrayAdapter<Customer> custAdapter;
    private Customer selection;

    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neworder1);
        getSupportActionBar().hide();
        branch = (Spinner) findViewById((R.id.br_spinner));
        customer = (AutoCompleteTextView) findViewById(R.id.act_cust);
        del_address = (EditText) findViewById(R.id.et_del_address);
        Del_date = (TextView) findViewById(R.id.DelDate);
        imgback=findViewById(R.id.imgback);
        getToken();
        ImageButton datebutton;
        datebutton = (ImageButton) findViewById(R.id.Dt_button);
        Button nxtbutton = (Button) findViewById(R.id.NxtButton);

        fillspinner();
        fillcustomer();
        Button clear= (Button) findViewById(R.id.BtnSearch);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customer.setText("");
            }
        });
        nxtbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Branch = ((Branch) branch.getSelectedItem()).getBcode();
                String Customer = null;
                if (selection != null) {
                    Customer = selection.getCdCustomer();
                }
                String deliveryAddress = del_address.getText().toString();
                String delvryDate = Del_date.getText().toString();
                if (!TextUtils.isEmpty(Branch)) {
                    if (!TextUtils.isEmpty(Customer)) {
                        if (!TextUtils.isEmpty(deliveryAddress)) {
                            if (!TextUtils.isEmpty(delvryDate)) {
                                Intent intent = new Intent(Neworder1.this, Neworder2.class);
                                intent.putExtra("Brname", Branch);
                                intent.putExtra("Cname", Customer);
                                intent.putExtra("DAddress", deliveryAddress);
                                intent.putExtra("DDate", delvryDate);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "please select delivery date", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "please select delivery address ", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "please select customer name ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "please select branch name ", Toast.LENGTH_SHORT).show();

                }

            }
        });
        Del_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment sf = new DatePickerFragment();
                sf.show(getSupportFragmentManager(), "date picker");
            }
        });
        datebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment sf = new DatePickerFragment();
                sf.show(getSupportFragmentManager(), "date picker");
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


    }

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    public void fillBranch() {
        spinnerArray_br = new ArrayList<Branch>();
        // spinnerArray_br.add("Branch1");
        //spinnerArray_br.add("Branch2");
        StringRequest sr = new StringRequest(Request.Method.GET, Br_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        // Log.d("Branch",brObject.getString("cd_branch"));
                        spinnerArray_br.add(new Branch(brObject.getString("cd_branch"), brObject.getString("ds_branch")));

                    }
                    ArrayAdapter<Branch> adapter = new ArrayAdapter<Branch>(
                            Neworder1.this, R.layout.spinner_item, spinnerArray_br);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    Spinner sItems = (Spinner) findViewById(R.id.br_spinner);
                    sItems.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtres = (TextView) findViewById(R.id.Res);
                txtres.setText(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
    }

    public void fillspinner() {
        fillBranch();
        //adapter.notifyDataSetChanged();
    }

    public void fillcustomer() {
        final ArrayList<Customer> custArray = new ArrayList<>();
        // custArray.add("Anu");
        //  custArray.add("sunil");
        String Cr_Url = "http://www.centroidsolutions.in/6_grace/api/get_customer.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        Customer customer = new Customer();
                        customer.setCdCustomer(brObject.getString("cd_customer"));
                        customer.setDsCustomer(brObject.getString("ds_customer"));
                        customer.setDsPhoneNo(brObject.getString("ds_phone_no"));
                        customer.setDsEmail(brObject.getString("ds_email"));
                        customer.setDsHouseName(brObject.getString("ds_house_name"));
                        customer.setDsStreetName(brObject.getString("ds_street_name"));
                        customer.setDsLocation(brObject.getString("ds_location"));
                        customer.setDsLocation(brObject.getString("ds_tin_number"));
                        Log.d("Customer", brObject.getString("ds_customer"));
                        custArray.add(customer);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtres = (TextView) findViewById(R.id.Res);
                txtres.setText(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
//********************************************************
        custAdapter = new ArrayAdapter<Customer>(this, android.R.layout.simple_list_item_1, custArray);
        customer.setAdapter(custAdapter);
        customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection = (Customer) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
            }
        });

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, i);
        c.set(Calendar.MONTH, i1);
        c.set(Calendar.DAY_OF_MONTH, i2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String Del_date = dateFormat.format(c.getTime());
        TextView del_date_text = (TextView) findViewById(R.id.DelDate);
        del_date_text.setText(Del_date);


    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }
}
