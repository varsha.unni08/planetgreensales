package com.example.appleapple.sampleapp1.model;

public class OrderInfo{
	private String vlGrandTotal;
	private String vlBalance;
	private String vlPaidAmount;
	private String dsSalesInvoice;
	private String dtProductSale;
	private String cdSalesOrder;

	public void setVlGrandTotal(String vlGrandTotal){
		this.vlGrandTotal = vlGrandTotal;
	}

	public String getVlGrandTotal(){
		return vlGrandTotal;
	}

	public void setVlBalance(String vlBalance){
		this.vlBalance = vlBalance;
	}

	public String getVlBalance(){
		return vlBalance;
	}

	public void setVlPaidAmount(String vlPaidAmount){
		this.vlPaidAmount = vlPaidAmount;
	}

	public String getVlPaidAmount(){
		return vlPaidAmount;
	}

	public void setDsSalesInvoice(String dsSalesInvoice){
		this.dsSalesInvoice = dsSalesInvoice;
	}

	public String getDsSalesInvoice(){
		return dsSalesInvoice;
	}

	public void setDtProductSale(String dtProductSale){
		this.dtProductSale = dtProductSale;
	}

	public String getDtProductSale(){
		return dtProductSale;
	}

	public void setCdSalesOrder(String cdSalesOrder){
		this.cdSalesOrder = cdSalesOrder;
	}

	public String getCdSalesOrder(){
		return cdSalesOrder;
	}

	@Override
 	public String toString(){
		return 
			"OrderInfo{" + 
			"vl_grand_total = '" + vlGrandTotal + '\'' + 
			",vl_balance = '" + vlBalance + '\'' + 
			",vl_paid_amount = '" + vlPaidAmount + '\'' + 
			",ds_sales_invoice = '" + dsSalesInvoice + '\'' + 
			",dt_product_sale = '" + dtProductSale + '\'' + 
			",cd_sales_order = '" + cdSalesOrder + '\'' + 
			"}";
		}
}
