package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.adapters.RecyclerAdapterBranch;
import com.example.appleapple.sampleapp1.adapters.RecyclerAdapterBranchOutstanding;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Order;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BranchOutstanding extends AppCompatActivity {

    ArrayList data = new ArrayList<Order>();

    String Token;
    private AutoCompleteTextView customer;
    private Branch selection1;
    private ArrayAdapter<Branch> custAdapter;
    private AbstractList<Branch> spinnerArray_br;
    final String Br_Url = Constants.base_url+"all_branches.php ";

    ImageView imgback;

    TextView txtname;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cancel_order);
        getSupportActionBar().hide();
        customer = (AutoCompleteTextView) findViewById(R.id.customer);
       // TextView cname = (TextView) findViewById(R.id.cname);
        imgback=findViewById(R.id.imgback);
        txtname=findViewById(R.id.txtname);
//        cname.setVisibility(View.GONE);
        getToken();
        customer.setVisibility(View.GONE);
        FillOrderData();
        Button clear = (Button) findViewById(R.id.BtnSearch);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customer.setText("");
            }
        });
        clear.setVisibility(View.GONE);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtname.setText("Branch Outstanding");

    }


    public void FillOrderData() {
        final ArrayList<Branch> branchs = new ArrayList<>();
        String Cr_Url = "http://www.centroidsolutions.in/6_grace/api/branch_outstanding.php";
        Log.v("TAG", "-->" + Cr_Url);
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONObject jsonObject = new JSONObject(response);
                    TextView tv = (TextView) findViewById(R.id.total);
                    tv.setText("Total balance : RS " + jsonObject.getString("Total"));
                    JSONArray res = jsonObject.getJSONArray("details");
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name, id;
                        name = brObject.getString("ds_branch");
                        id = brObject.getString("cd_branch");
                        stk = brObject.getString("vl_balance");
                        System.out.println(name + stk);
                        branchs.add(new Branch(id, name, stk));

                    }
                    RecyclerView orderlist = (RecyclerView) findViewById(R.id.Orderslist);
                    RecyclerAdapterBranchOutstanding adapter = new RecyclerAdapterBranchOutstanding(branchs, BranchOutstanding.this);
                    orderlist.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No data present", Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //************************************************
    }

    public void fillBranch() {
        spinnerArray_br = new ArrayList<Branch>();
        // spinnerArray_br.add("Branch1");
        //spinnerArray_br.add("Branch2");
        StringRequest sr = new StringRequest(Request.Method.GET, Br_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        // Log.d("Branch",brObject.getString("cd_branch"));
                        spinnerArray_br.add(new Branch(brObject.getString("cd_branch"), brObject.getString("ds_branch")));

                    }
                    custAdapter = new ArrayAdapter<Branch>(BranchOutstanding.this, android.R.layout.simple_list_item_1, spinnerArray_br);
                    customer.setAdapter(custAdapter);
                    customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                            selection1 = (Branch) parent.getItemAtPosition(position);

                            //TODO Do something with the selected text
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                TextView txtres = (TextView) findViewById(R.id.Res);
                txtres.setText(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
    }


}
