package com.example.appleapple.sampleapp1.model;



public class Customer {

    private String cdCustomer;
    private String dsCustomer;
    private String dsPhoneNo;
    private String dsEmail;
    private String dsHouseName;
    private String dsStreetName;
    private String dsLocation;
    private String dsTinNumber;

    /**
     * No args constructor for use in serialization
     */
    public Customer() {
    }

    /**
     * @param dsLocation
     * @param dsPhoneNo
     * @param dsCustomer
     * @param dsStreetName
     * @param dsEmail
     * @param dsHouseName
     * @param dsTinNumber
     * @param cdCustomer
     */
    public Customer(String cdCustomer, String dsCustomer, String dsPhoneNo, String dsEmail, String dsHouseName, String dsStreetName, String dsLocation, String dsTinNumber) {
        super();
        this.cdCustomer = cdCustomer;
        this.dsCustomer = dsCustomer;
        this.dsPhoneNo = dsPhoneNo;
        this.dsEmail = dsEmail;
        this.dsHouseName = dsHouseName;
        this.dsStreetName = dsStreetName;
        this.dsLocation = dsLocation;
        this.dsTinNumber = dsTinNumber;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getDsPhoneNo() {
        return dsPhoneNo;
    }

    public void setDsPhoneNo(String dsPhoneNo) {
        this.dsPhoneNo = dsPhoneNo;
    }

    public String getDsEmail() {
        return dsEmail;
    }

    public void setDsEmail(String dsEmail) {
        this.dsEmail = dsEmail;
    }

    public String getDsHouseName() {
        return dsHouseName;
    }

    public void setDsHouseName(String dsHouseName) {
        this.dsHouseName = dsHouseName;
    }

    public String getDsStreetName() {
        return dsStreetName;
    }

    public void setDsStreetName(String dsStreetName) {
        this.dsStreetName = dsStreetName;
    }

    public String getDsLocation() {
        return dsLocation;
    }

    public void setDsLocation(String dsLocation) {
        this.dsLocation = dsLocation;
    }

    public String getDsTinNumber() {
        return dsTinNumber;
    }

    public void setDsTinNumber(String dsTinNumber) {
        this.dsTinNumber = dsTinNumber;
    }

    @Override
    public String toString() {
        return this. dsCustomer;
    }
}