package com.example.appleapple.sampleapp1.adapters;


import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.appleapple.sampleapp1.fragments.BranchOutstandFragment;
import com.example.appleapple.sampleapp1.fragments.CustomerOutStandFragment;

public class OutStandingTabpagerAdapter extends FragmentPagerAdapter {

    String arr[]={"Customer","Branch"};

    public OutStandingTabpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if(position==0)
        {
            fragment=new CustomerOutStandFragment();
        }
        else {

            fragment=new BranchOutstandFragment();
        }


        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return arr[position];
    }
}
