package com.example.appleapple.sampleapp1.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.BranchOutstanding;
import com.example.appleapple.sampleapp1.CustomerOutstanding;
import com.example.appleapple.sampleapp1.MainActivity;
import com.example.appleapple.sampleapp1.NewCustomer;
import com.example.appleapple.sampleapp1.Neworder1;
import com.example.appleapple.sampleapp1.Payment;
import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.TargetActivity;
import com.example.appleapple.sampleapp1.ViewOrder;
import com.example.appleapple.sampleapp1.ViewStock;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

public class HomeActivity extends AppCompatActivity  implements NetworkChangeReceiver.ConnectivityReceiverListener  {

    private Snackbar mSnackBar;

    ImageView imgLogout,imgarp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        mSnackBar = Snackbar.make(findViewById(android.R.id.content), "Please press again to exit!", Snackbar.LENGTH_SHORT);

        LinearLayout neworder=(LinearLayout) findViewById(R.id.new_orderBtn);
        imgLogout=findViewById(R.id.imgLogout);
        imgarp=findViewById(R.id.imgarp);

        imgarp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ARPActivity.class));
            }
        });


        neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, Neworder1.class));
            }
        });
      LinearLayout newcus=(LinearLayout)findViewById(R.id.newCustomer);

        newcus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, NewCustomer.class));
            }
        });

        LinearLayout target=( LinearLayout)findViewById(R.id.vwtarget);

        target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, TargetActivity.class));
            }
        });

        LinearLayout vwStock=( LinearLayout)findViewById(R.id.vwStockBtn);

        vwStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ViewStock.class));
            }
        });





        LinearLayout canButton=( LinearLayout)findViewById(R.id.layout_collection);
        canButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, CollectionsActivity.class));
            }
        });






        LinearLayout viewButton=( LinearLayout)findViewById(R.id.view_orderBtn);
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ViewOrder.class));

            }
        });
        LinearLayout editButton=( LinearLayout)findViewById(R.id.Sales);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SalesActivity.class));




            }
        });

        LinearLayout paymentButton=( LinearLayout)findViewById(R.id.payment);
        paymentButton.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 startActivity(new Intent(HomeActivity.this, Payment.class));
                                             }
                                         }
        );
        LinearLayout branchoutstannding=( LinearLayout)findViewById(R.id.branchoutstanding);
        branchoutstannding.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 startActivity(new Intent(HomeActivity.this, OutstandingActivity.class));
                                             }
                                         }
        );

        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
                alertDialogBuilder.setMessage("Do you want to logout?");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                startIntent(HomeActivity.this,arg0);
                            }
                        });

                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialogBuilder.show();

            }
        });
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.edit_menu, menu);
//        return true;
//    }

    @Override
    public void onBackPressed() {
        if (mSnackBar.isShown()) {
            Log.v("TAG", "v1");
          HomeActivity.this.finish();

        } else {
            Log.v("TAG", "v2");
            mSnackBar.show();
        }}


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.menu_edit_user:
////                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
////                alertDialogBuilder.setMessage("Do you want to logout?");
////                        alertDialogBuilder.setPositiveButton("yes",
////                                new DialogInterface.OnClickListener() {
////                                    @Override
////                                    public void onClick(DialogInterface arg0, int arg1) {
////                                        startIntent(HomeActivity.this,arg0);
////                                    }
////                                });
////
////                alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                       dialog.dismiss();
////                    }
////                });
////
////                alertDialogBuilder.show();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
    private void startIntent(HomeActivity activity,DialogInterface d) {
        SharedPreferences sharedpref=getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedpref.edit();
        editor.putString("token",null);
        editor.apply();
       d.dismiss();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }

}
