package com.example.appleapple.sampleapp1;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.activities.HomeActivity;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Product;
import com.example.appleapple.sampleapp1.model.ProductItem;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDetails extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener, RecyclerViewAdapter.IUpdateEvent {

    public static final String CALL = Constants.base_url+"order_details.php";
    EditText  tv_soid;
   TextView tv_cname;
    EditText address;
    TextView tv_del_date, tv_pay_status, tv_del_status;
    EditText tv_total, tvBalance;
    LinearLayout orderUpdate;
    String dtDelivery;
    String grandTotal;
    String ds_delivery_address;
    String customer;
    List<Itemclass> itemList;
    Button choose;
    Integer cgst = null;
    Integer sgst = null;
    Integer stockQty = null;
    String Branch, C_name, CAddress, Del_date;
    ArrayList<Product> prds = new ArrayList<Product>();
    int Totalamt = 0;
    String stock, upr;
    SearchableSpinner pname;
    String Token;
    ArrayList<String> PrdnamesArray = new ArrayList<String>();
    String OrderId;
    ArrayList<Itemclass> items = new ArrayList<Itemclass>();
    private RecyclerViewAdapter adapter;

    ImageView imgback;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__details);

        getSupportActionBar().hide();
        getToken();
        tv_soid = (EditText) findViewById(R.id.tv_soid);
        tv_cname = (TextView) findViewById(R.id.tv_cname);
        orderUpdate = (LinearLayout) findViewById(R.id.orderUpdate);
        address = (EditText) findViewById(R.id.address);
        tv_del_date = (TextView) findViewById(R.id.tv_del_date);
        tv_total = (EditText) findViewById(R.id.tv_Total);
        tv_pay_status = (TextView) findViewById(R.id.tv_pay_status);
        tv_del_status = (TextView) findViewById(R.id.tv_del_status);
        tvBalance = (EditText) findViewById(R.id.tv_balance);
        choose = (Button) findViewById(R.id.addnew);
        imgback=findViewById(R.id.imgback);


        try {
            Intent intent = getIntent();
            OrderId = intent.getStringExtra("OrderId");
            Branch = intent.getStringExtra("branch");
            String cus = intent.getStringExtra("cus");
            tv_cname.setText(""+cus);
            if (NetworkChangeReceiver.isConnected()) {
                getdata(OrderId);
            }
            tv_del_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePickerDialog(1);
                }
            });
            choose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder mbuilder = new AlertDialog.Builder(OrderDetails.this);
                    final View mview = getLayoutInflater().inflate(R.layout.chooseitem_dialog, null);
                    mbuilder.setView(mview);
                    pname = (SearchableSpinner) mview.findViewById(R.id.act_prd);
                    fillProduct();
                    mbuilder.setTitle("Choose Item");
                    final EditText uprice = (EditText) mview.findViewById(R.id.tv_unitPrice);
                    final EditText Total = (EditText) mview.findViewById(R.id.txt_PTotal);
                    final EditText Tqty = (EditText) mview.findViewById(R.id.txt_qty);
                    Tqty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View view, boolean b) {
                            try {
                                int qty = Integer.parseInt(Tqty.getText().toString());
                                int upr = Integer.parseInt(uprice.getText().toString());
                                if (upr != 0) {
                                    Integer totVal = qty * upr;
                                    double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                    Total.setText(String.valueOf((int) currentTotl));
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please enter unit price", Toast.LENGTH_SHORT).show();
                                }

                            } catch (NumberFormatException ex) {
                            }


                        }
                    });
                    uprice.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                            try {
//                                int qty = Integer.parseInt(Tqty.getText().toString());
//                                int upr = Integer.parseInt(uprice.getText().toString());
//                                if (upr > 0) {
//                                    Integer totVal = qty * upr;
//                                    double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
//                                    Total.setText(String.valueOf((int) currentTotl));
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Please enter unit price", Toast.LENGTH_SHORT).show();
//                                }
//
//                            } catch (NumberFormatException ex) {
//                            }
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            try {
                                if (!TextUtils.isEmpty(Tqty.getText().toString())) {
                                    int qty = Integer.parseInt(Tqty.getText().toString());
                                    double upr = (Double.parseDouble(uprice.getText().toString()));
                                    if (upr > 0.0) {
                                        Integer totVal = (int) (qty * upr);
                                        double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                        Total.setText(String.valueOf((int) currentTotl));
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please enter unit price", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    Tqty.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                            try {
//                                int qty = Integer.parseInt(Tqty.getText().toString());
//                                int upr = Integer.parseInt(uprice.getText().toString());
//                                if (upr > 0) {
//                                    Integer totVal = qty * upr;
//                                    double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
//                                    Total.setText(String.valueOf((int) currentTotl));
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Please enter unit price", Toast.LENGTH_SHORT).show();
//                                }
//
//                            } catch (NumberFormatException ex) {
//                            }
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            try {
                                if (!TextUtils.isEmpty(Tqty.getText().toString())) {
                                    int qty = Integer.parseInt(Tqty.getText().toString());
                                    double upr = (Double.parseDouble(uprice.getText().toString()));
                                    if (upr > 0.0) {
                                        Integer totVal = (int) (qty * upr);
                                        double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                        Total.setText(String.valueOf((int) currentTotl));
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please enter unit price", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                                           /*uprice.set(new View.OnFocusChangeListener() {
                                               @Override
                                               public void onFocusChange(View view, boolean b) {



                                               }
                                           });*/
                    pname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            String item = ((ProductItem) pname.getSelectedItem()).getCd_product();
                            //****************
                            String Cr_Url = Constants.base_url+"product_details.php?cd_product=" + item + "&cd_branch=" + Branch;
                            System.out.println(Cr_Url);
                            StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONArray res = new JSONArray(response);
                                        //getting the json object of the particular index inside the array
                                        JSONObject brObject = res.getJSONObject(0);
                                        //String Product = brObject.getString("ds_product");
                                        String Uprice = brObject.getString("vl_sell_price");
                                        String stock = brObject.getString("stock");
                                        TextView stockview = mview.findViewById(R.id.tv_stock);
                                        TextView uprice = mview.findViewById(R.id.tv_unitPrice);
                                        TextView Tv_pname = (TextView) mview.findViewById(R.id.pname);
                                        //Tv_pname.setText(Product);
                                        sgst = brObject.getInt("vl_sgst");
                                        cgst = brObject.getInt("vl_cgst");
                                        stockview.setText(stock);
                                        uprice.setText(Uprice);
                                        System.out.println(brObject.toString());


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    TextView txtres = (TextView) findViewById(R.id.Res);
                                    txtres.setText(error.toString());
                                }
                            }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<String, String>();
                                    headers.put("Authorization", Token);
                                    return headers;
                                }


                            };
                            RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
                            reque.add(sr);
                            //*****************
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                    Button btn_cancel = (Button) mview.findViewById(R.id.can_prd);
//**********************************************************************************************************************************
                    Button btnprd = (Button) mview.findViewById(R.id.btn_add_prd);
                    mbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    mbuilder.setPositiveButton("Add Product", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            JSONObject objorderdata = new JSONObject();
                            EditText Tqty = (EditText) mview.findViewById(R.id.txt_qty);
                            Spinner prd = (Spinner) mview.findViewById(R.id.act_prd);
                            EditText Total = (EditText) mview.findViewById(R.id.txt_PTotal);
                            TextView Tv_Stock = (TextView) mview.findViewById(R.id.tv_stock);
                            TextView Tv_UnitPrice = (TextView) mview.findViewById(R.id.tv_unitPrice);
                            TextView Tv_pname = (TextView) mview.findViewById(R.id.pname);
                            int stock, qnty;
                            stock = Integer.parseInt(Tv_Stock.getText().toString());
                            qnty = Integer.parseInt(Tqty.getText().toString());
//                            if (qnty > stock) {
//                                Toast.makeText(getApplicationContext(), "Less Stock available", Toast.LENGTH_LONG);
//                                Tqty.setText("0");
//                            } else {
                            String pid = ((ProductItem) prd.getSelectedItem()).getCd_product();
                            String pname = Tv_pname.getText().toString();
                            String qty = Tqty.getText().toString();
                            String pTotal = Total.getText().toString();
                            String Uprice = Tv_UnitPrice.getText().toString();
                            // Product selPrd = new Product(pid, pname, Integer.parseInt(pTotal), Integer.parseInt(qty), Integer.parseInt(Uprice));
                            // prds.add(selPrd);
                            Totalamt = Totalamt + Integer.parseInt(pTotal);
                            itemList.add(new Itemclass(((ProductItem) prd.getSelectedItem()).getDs_product(), ((ProductItem) prd.getSelectedItem()).getCd_product(), qty, String.valueOf(cgst), String.valueOf(sgst), pTotal, Uprice));
                            updateTotal(Totalamt);
                            updateList(itemList);
                            dialogInterface.dismiss();
                            //}
                        }
                    });
                    final AlertDialog dialog = mbuilder.create();
                    dialog.show();
                    dialog.setTitle("Choose item");
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.getWindow().setAttributes(lp);


                }
            });
            orderUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!TextUtils.isEmpty(tv_cname.getText().toString())) {
                        if (!TextUtils.isEmpty(address.getText().toString())) {
                            if (!TextUtils.isEmpty(tv_del_date.getText().toString())) {
                                if (!TextUtils.isEmpty(tv_total.getText().toString())) {
                                    placeOrder(OrderId);
                                } else {
                                    Toast.makeText(getApplicationContext(), "invalid total amount", Toast.LENGTH_LONG).show();

                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "invalid delivery date", Toast.LENGTH_LONG).show();

                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "invalid address ", Toast.LENGTH_LONG).show();

                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "invalid customer id", Toast.LENGTH_LONG).show();

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void getdata(final String orderId) {
        String Cr_Url = Constants.base_url+"order_details.php?cd_sales_order=" + orderId;
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //getting the json object of the particular index inside the array
                    JSONObject brObject = new JSONObject(response);
                    tv_soid.setText("" + orderId);
                    //tv_cname.setText("" + brObject.getString("cd_customer"));
                    address.setText("" + brObject.getString("ds_delivery_address"));
                    tv_total.setText("" + brObject.getString("vl_total"));
                    tv_del_date.setText("" + brObject.getString("dt_record"));
                    tvBalance.setText("" + brObject.getString("vl_balance"));
                    if (brObject.getString("ds_delivery_status").equalsIgnoreCase("0")) {
                        tv_del_status.setText("Status : Not delivered");
                    } else {
                        tv_del_status.setText("Status : Delivered");
                    }
                    if (brObject.getString("ds_payment_status").equalsIgnoreCase("0")) {
                        tv_del_status.setText("Payment : incomplete");
                    } else {
                        tv_del_status.setText("Payment : Completed");
                    }
                    JSONArray jsonArray = brObject.getJSONArray("items");
                    itemList = new ArrayList<>();
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject itemObject = jsonArray.getJSONObject(i);
                            itemList.add(new Itemclass(itemObject.getString("ds_product"), itemObject.getString("cd_product"), itemObject.getString("nr_qty"), itemObject.getString("vl_cgst"), itemObject.getString("vl_sgst"), itemObject.getString("vl_total"), itemObject.getString("vl_rate")));
                        }

                    }
                    updateList(itemList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }


    public void placeOrder(final String orderId) {
        JSONObject objOrderdata = new JSONObject();
        JSONObject obj;
        JSONArray orderedItems = new JSONArray();
        try {
            List<Itemclass> prds = adapter.getItems();
            objOrderdata.put("cd_sales_order", orderId);
            objOrderdata.put("ds_delivery_address", address.getText().toString());
            objOrderdata.put("dt_delivery", tv_del_date.getText().toString());
            objOrderdata.put("grand_total", tv_total.getText().toString());
            if (prds != null) {
                for (int i = 0; i < prds.size(); i++) {
                    Itemclass p = prds.get(i);
                    obj = new JSONObject();
                    obj.put("cd_product", p.getItemId());
                    obj.put("nr_qty", p.getQty());
                    obj.put("vl_total", p.getTotal());
                    orderedItems.put(i, obj);
                }
                objOrderdata.put("saleitems", orderedItems);
                String Cr_Url = Constants.base_url+"update_sales_order.php";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        com.android.volley.Request.Method.POST, Cr_Url,
                        objOrderdata,
                        new com.android.volley.Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String orderNo = "0";
                                    Log.v("Tag", "sdkdks");
                                    System.out.println("*********************************");
                                   // JSONArray jsonArray = new JSONArray(response);
                                    if(response.getString("status").equalsIgnoreCase("1")){
                                    //  if (jsonArray.getJSONObject(0).getString("status").equalsIgnoreCase("1")) {
                                    Toast.makeText(getApplicationContext(), "Order updated Successfully", Toast.LENGTH_LONG).show();
                                    startActivity(new Intent(OrderDetails.this, HomeActivity.class));
                                    finish();}else{
                                        Toast.makeText(getApplicationContext(), "Order updated failed.Please try after some time.", Toast.LENGTH_LONG).show();


                                    }
                                    // } else
                                    // Toast.makeText(getApplicationContext(), "Some thing went wrong.Please try again", Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.print(error.toString());
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("Authorization", Token);
                        return headers;
                    }

                };
                RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
                reque.add(jsonObjectRequest);
            }
            System.out.println(objOrderdata.toString());


        } catch (JSONException e) {
            // System.out.println("error");
            e.printStackTrace();
        }
        //**************************************************************
//voley code begins
// **************************************************************
    }

    private void showDatePickerDialog(int i) {
        try {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    Calendar cal = Calendar.getInstance();
                    cal.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String fDate = df.format(cal.getTime());
                    tv_del_date.setText(fDate);
                }

            }
                    , mYear, mMonth, mDay);
            datePickerDialog.show();
        } catch (Exception e) {
        }
    }


    public void fillProduct() {
        PrdnamesArray.clear();
        //  PrdnamesArray.add("Product1");
        // PrdnamesArray.add("Product3");
        // PrdnamesArray.add("Product5");
        //**************************************************
        String Cr_Url = Constants.base_url+"all_products.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    List<ProductItem> list = new ArrayList<>();
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        System.out.println(brObject.getString("cd_product"));
                        list.add(new ProductItem(brObject.getString("cd_product"), brObject.getString("ds_product")));

                    }
                    ArrayAdapter<ProductItem> prdAdapter = new ArrayAdapter<ProductItem>(OrderDetails.this, android.R.layout.simple_spinner_item, list);
                    pname.setAdapter(prdAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //************************************************
    }

    private void updateTotal(int totalamt) {

        tv_total.setText( String.valueOf(Totalamt));

    }

    public void updateList(List<Itemclass> itemList1) {
        RecyclerView list = (RecyclerView) findViewById(R.id.itemlist);

        adapter = new RecyclerViewAdapter(itemList1, OrderDetails.this);
        list.setAdapter(adapter);

        if(adapter!=null){
            adapter.setIUpateEvent(this);
        }

        if(itemList1!=null){
            Log.d("kj", "kjkjkj");
            Double sum=0.0;
            for(Itemclass itemclass:itemList1){
                sum=sum+Double.parseDouble(itemclass.getTotal());

            }
            tv_total.setText(""+new DecimalFormat("#,###,###.00").format(sum));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }


    @Override
    public void updateTotal(List<Itemclass> items) {
        try{
        if(items!=null){
            Log.d("kj", "kjkjkj");
            Double sum=0.0;
          for(Itemclass itemclass:items){
              sum=sum+Double.parseDouble(itemclass.getTotal());

          }
          tv_total.setText(""+new DecimalFormat("#,###,###.00").format(sum));
        }}catch (Exception e){
            e.printStackTrace();
        }
    }
}
