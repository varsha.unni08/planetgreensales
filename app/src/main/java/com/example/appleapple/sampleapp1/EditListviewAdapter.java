package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Order;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditListviewAdapter extends ArrayAdapter<Order> {

    private final ArrayList<Order> orders;
    Context mcontext;
    int mresource;
    String Token;

    public void getToken(View v) {
        SharedPreferences sharedpref = v.getContext().getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    public EditListviewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Order> objects) {
        super(context, resource, objects);
        mcontext = context;
        mresource = resource;
        this.orders = objects;


    }

    static class ViewHolder {
        private TextView orderId;
        private TextView custId;
        private Button btnEdit;
        private ImageView btnCancel;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {


      /*  String id=getItem(position).getOrderId();
        String cname=getItem(position).getCustomername();
        Order od=new Order(id,cname);
        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView orderid=(TextView) convertView.findViewById(R.id.tv_oid);
        TextView cust=(TextView) convertView.findViewById(R.id.tv_cname);
        orderid.setText("Order id:"+id);
        cust.setText("Customer "+cname);*/
        // return  convertView;
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflate = LayoutInflater.from(mcontext);
            convertView = mInflate.inflate(mresource, null);
            holder = new ViewHolder();
            holder.orderId = (TextView) convertView.findViewById(R.id.tv_oid);
            holder.custId = (TextView) convertView.findViewById(R.id.tv_cname);
            holder.btnEdit = (Button) convertView.findViewById(R.id.btn_edit);
            holder.btnCancel = (ImageView) convertView.findViewById(R.id.can_order);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Order orderItem = getItem(position);
        holder.orderId.setText("Order id:" + orderItem.getOrderId());
        holder.custId.setText("Customer " + orderItem.getCustomername());
    holder.btnEdit.setVisibility(View.GONE);
        holder.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(getItem(position),position);
            }
        });
        return convertView;

    }

    public void CancelOrder(final String oid, final String reason, final int pos) {
        String url = Constants.base_url+"cancel_order.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    System.out.println(response);
                    orders.remove(pos);
                    notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cd_sales_order", oid);
                params.put("reason", reason);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(mcontext);
        reque.add(sr);


    }

    private void showDialog(final Order orderItem,final int pos) {
        final android.support.v7.app.AlertDialog.Builder mbuilder = new android.support.v7.app.AlertDialog.Builder(mcontext);
        LayoutInflater inflater = LayoutInflater.from(mcontext);
        final View mview = inflater.inflate(R.layout.delete_orderdialog, null);
        mbuilder.setView(mview);

        final android.support.v7.app.AlertDialog dialog = mbuilder.create();
        dialog.show();
        Button btndelete = (Button) mview.findViewById(R.id.deletebtn);
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oid = orderItem.getOrderId();
                // String oid="1";
                EditText txtRsn = (EditText) mview.findViewById(R.id.ReasonTxt);
                String reason = txtRsn.getText().toString();
                getToken(view);
                CancelOrder(oid, reason,pos);
                dialog.dismiss();

            }
        });
        Button btncancel = (Button) mview.findViewById(R.id.cancelbtn);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


    }
}
