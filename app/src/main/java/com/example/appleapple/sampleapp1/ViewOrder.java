package com.example.appleapple.sampleapp1;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Order;
import com.example.appleapple.sampleapp1.model.OrderItem;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ViewOrder extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {


    ArrayList<Order> all_orders = new ArrayList<Order>();
    ArrayList<String> ordertype = new ArrayList<String>();
    RecyclerView rcv;
    OrderRecyclerAdapter madapter;
    RecyclerView.LayoutManager mlayoutmansager;
    Spinner sItems;
    String apicall = "";

    TextView from, to;
    String Token;
    private ArrayList<OrderItem> itemList;
    Button search;
    ImageView imgback;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_order);
        getSupportActionBar().hide();
        getToken();
        //*******************
        ordertype.add("Placed Orders");
        ordertype.add("Approved Orders");
        ordertype.add("Payment pending orders");
        ordertype.add("Cancelled");
        sItems = (Spinner) findViewById(R.id.Order_type);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        search = (Button) findViewById(R.id.search);

        imgback=findViewById(R.id.imgback);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, ordertype);
        sItems.setAdapter(adapter);
        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                             @Override
                                             public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                 if (!TextUtils.isEmpty(from.getText().toString())) {
                                                     if (!TextUtils.isEmpty(to.getText().toString())) {
                                                         if (NetworkChangeReceiver.isConnected()) {
                                                             Filldata(from.getText().toString(), to.getText().toString());
                                                         }
                                                     } else {
                                                         Filldata();
                                                     }
                                                 } else {
                                                     Filldata();
                                                 }


                                             }

                                             @Override
                                             public void onNothingSelected(AdapterView<?> adapterView) {
                                             }
                                         }
        );
        //*********************************
        rcv = (RecyclerView) findViewById(R.id.rec_view);
        rcv.setHasFixedSize(true);
        mlayoutmansager = new LinearLayoutManager(this);
        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(1);
            }
        });
        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(2);
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(from.getText().toString())) {
                    if (!TextUtils.isEmpty(to.getText().toString())) {
                        if (NetworkChangeReceiver.isConnected()) {
                            Filldata(from.getText().toString(), to.getText().toString());
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please select to date ", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select from date ", Toast.LENGTH_SHORT).show();

                }
            }
        });
        FillOrderType();

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


    }


    private void FillOrderType() {
    }

    private void Filldata() {
        String url = "";
        if (sItems.getSelectedItemPosition() == 0) {
            System.out.println(String.valueOf("hello"));
            url = Constants.base_url+ "view_orders.php?apicall=placed";
            apicall = "placed";
        }
        if (sItems.getSelectedItemPosition() == 1) {
            url = Constants.base_url+"view_orders.php?apicall=approved";
            apicall = "approved";
        }
        if (sItems.getSelectedItemPosition() == 2) {
            url = Constants.base_url+"view_orders.php?apicall=delivered";
            apicall = "delivered";
        }
        if (sItems.getSelectedItemPosition() == 3) {
            url = Constants.base_url+"view_orders.php?apicall=canceled";
            apicall = "delivered";
        }
        //****************************************************************
        StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



            /*    try {

                    System.out.println(response);
                    JSONArray res=new JSONArray(response);

                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);



                        String id,name;
                        id=brObject.getString("cd_sales_order");
                        name=brObject.getString("cd_customer");

                        System.out.println(id+name);
                        all_orders.add(new Order(id,name));



                    }


                    madapter= new OrderRecyclerAdapter(all_orders);
                    rcv.setLayoutManager(mlayoutmansager);
                    rcv.setAdapter(madapter);
                    madapter.SetOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {
                        @Override
                        public void OnItemClick(int position) {

                            Toast.makeText(getApplicationContext(),all_orders.get(position).OrderId.toString(),Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(ViewOrder.this,OrderDetails.class);
                            intent.putExtra("OrderId",all_orders.get(position).OrderId.toString());
                            startActivity(intent);
                        }
                    });

                } catch (JSONException e) {
                    madapter=null;
                    rcv.setAdapter(madapter);
                }


*/
                try {
                    //getting the json object of the particular index inside the array
                    JSONArray jsonArray = new JSONArray(response);
                    itemList = new ArrayList<>();
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject itemObject = jsonArray.getJSONObject(i);
                            OrderItem orderItem = new OrderItem();
                            orderItem.setCdSalesOrder(itemObject.getString("cd_sales_order"));
                            orderItem.setCdBranch(itemObject.getString("cd_branch"));
                            orderItem.setDtDelivery(itemObject.getString("dt_delivery"));
                            orderItem.setDsCustomer(itemObject.getString("ds_customer"));
                            orderItem.setCdCustomer(itemObject.getString("cd_customer"));
                            orderItem.setDsDeliveryAddress(itemObject.getString("ds_delivery_address"));
                            orderItem.setVlTotal(itemObject.getString("vl_total"));
                            itemList.add(orderItem);
                        }
                        madapter = new OrderRecyclerAdapter(itemList, ViewOrder.this);
                        rcv.setLayoutManager(mlayoutmansager);
                        rcv.setAdapter(madapter);
                        madapter.SetOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {
                            @Override
                            public void OnItemClick(int position) {
                                if (sItems.getSelectedItemPosition() == 0) {
                                    Intent intent = new Intent(ViewOrder.this, OrderDetails.class);
                                    intent.putExtra("OrderId", itemList.get(position).getCdSalesOrder());
                                    intent.putExtra("branch", itemList.get(position).getCdBranch());
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Approved or pending orders cannot be edited!.", Toast.LENGTH_LONG).show();

                                }
                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("apicall", apicall);
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //*****************************************************************
    }

    private void Filldata(final String from, final String to) {
        String url = "";
        if (sItems.getSelectedItemPosition() == 0) {
            System.out.println(String.valueOf("hello"));
            url = Constants.base_url+"view_orders.php?apicall=placed&from=" + from + "&to=" + to;
            apicall = "placed";
        }
        if (sItems.getSelectedItemPosition() == 1) {
            url = Constants.base_url+"view_orders.php?apicall=approved&from=" + from + "&to=" + to;
            apicall = "approved";
        }
        if (sItems.getSelectedItemPosition() == 2) {
            url = Constants.base_url+"view_orders.php?apicall=delivered&from=" + from + "&to=" + to;
            apicall = "delivered";
        }
        if (sItems.getSelectedItemPosition() == 3) {
            url = Constants.base_url+"view_orders.php?apicall=canceled&from=" + from + "&to=" + to;
            apicall = "delivered";
        }
        //****************************************************************
        StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



            /*    try {

                    System.out.println(response);
                    JSONArray res=new JSONArray(response);

                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);



                        String id,name;
                        id=brObject.getString("cd_sales_order");
                        name=brObject.getString("cd_customer");

                        System.out.println(id+name);
                        all_orders.add(new Order(id,name));



                    }


                    madapter= new OrderRecyclerAdapter(all_orders);
                    rcv.setLayoutManager(mlayoutmansager);
                    rcv.setAdapter(madapter);
                    madapter.SetOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {
                        @Override
                        public void OnItemClick(int position) {

                            Toast.makeText(getApplicationContext(),all_orders.get(position).OrderId.toString(),Toast.LENGTH_LONG).show();
                            Intent intent=new Intent(ViewOrder.this,OrderDetails.class);
                            intent.putExtra("OrderId",all_orders.get(position).OrderId.toString());
                            startActivity(intent);
                        }
                    });

                } catch (JSONException e) {
                    madapter=null;
                    rcv.setAdapter(madapter);
                }


*/
                try {
                    //getting the json object of the particular index inside the array
                    JSONArray jsonArray = new JSONArray(response);
                    itemList = new ArrayList<>();
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject itemObject = jsonArray.getJSONObject(i);
                            OrderItem orderItem = new OrderItem();
                            orderItem.setCdSalesOrder(itemObject.getString("cd_sales_order"));
                            orderItem.setCdBranch(itemObject.getString("cd_branch"));
                            orderItem.setDtDelivery(itemObject.getString("dt_delivery"));
                            orderItem.setDsCustomer(itemObject.getString("ds_customer"));
                            orderItem.setCdCustomer(itemObject.getString("cd_customer"));
                            orderItem.setDsDeliveryAddress(itemObject.getString("ds_delivery_address"));
                            orderItem.setVlTotal(itemObject.getString("vl_total"));
                            itemList.add(orderItem);
                        }
                        madapter = new OrderRecyclerAdapter(itemList, ViewOrder.this);
                        rcv.setLayoutManager(mlayoutmansager);
                        rcv.setAdapter(madapter);

                        rcv.setVisibility(View.VISIBLE);
                        madapter.SetOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {
                            @Override
                            public void OnItemClick(int position) {
                                if (sItems.getSelectedItemPosition() == 0) {
                                    Intent intent = new Intent(ViewOrder.this, OrderDetails.class);
                                    intent.putExtra("OrderId", itemList.get(position).getCdSalesOrder());
                                    intent.putExtra("branch", itemList.get(position).getCdBranch());
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Approved or pending orders cannot be edited!.", Toast.LENGTH_LONG).show();

                                }
                            }
                        });
                    } else {
                        rcv.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    rcv.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
                rcv.setVisibility(View.GONE);
                Toast.makeText(ViewOrder.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("apicall", apicall);
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //*****************************************************************
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

     /*   MenuInflater inlater=getMenuInflater();
        inlater.inflate(R.menu.,menu);
        MenuItem searchitem=menu.findItem(R.id.action_search);
        SearchView searchView=(SearchView)searchitem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                madapter.getFilter().filter(s.toString());
                return false;
            }
        });*/
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }

    private void showDatePickerDialog(final int i) {
        try {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                            Calendar cal = Calendar.getInstance();
                            cal.set(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String fDate = df.format(cal.getTime());
                            if (!TextUtils.isEmpty(fDate)) {
                                if (i == 1) {
                                    from.setText(fDate);
                                } else {
                                    to.setText(fDate);
                                }
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } catch (Exception e) {
        }
    }

}
