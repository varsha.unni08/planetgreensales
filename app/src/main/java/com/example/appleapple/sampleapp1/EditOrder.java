package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Order;
import com.example.appleapple.sampleapp1.model.OrderItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditOrder extends AppCompatActivity {


    ArrayList<Order> all_orders = new ArrayList<Order>();
    RecyclerView rcv;
    OrderRecyclerAdapter madapter;
    RecyclerView.LayoutManager mlayoutmansager;
    public static final String API = Constants.base_url+"view_orders.php?apicall=placed";
    private String token;

    ImageView imgback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorder);
        getSupportActionBar().hide();
        rcv = (RecyclerView) findViewById(R.id.rec_view);
        imgback=findViewById(R.id.imgback);
        rcv.setHasFixedSize(true);
        mlayoutmansager = new LinearLayoutManager(this);
        try {
            SharedPreferences sharedpref=getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
            token="Bearer "+sharedpref.getString("token","not null");
            getdata();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

    }

    private void getdata() {
        StringRequest sr = new StringRequest(Request.Method.GET, API, new Response.Listener<String>() {
            private ArrayList<OrderItem> itemList;

            @Override
            public void onResponse(String response) {
                try {
                    //getting the json object of the particular index inside the array
                    JSONArray jsonArray = new JSONArray(response);
                    itemList = new ArrayList<>();
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject itemObject = jsonArray.getJSONObject(i);
                            OrderItem orderItem = new OrderItem();
                            orderItem.setCdSalesOrder(itemObject.getString("cd_sales_order"));
                            orderItem.setCdBranch(itemObject.getString("cd_branch"));
                            orderItem.setDtDelivery(itemObject.getString("dt_delivery"));
                            orderItem.setDsCustomer(itemObject.getString("ds_customer"));
                            orderItem.setCdCustomer(itemObject.getString("cd_customer"));
                            orderItem.setDsDeliveryAddress(itemObject.getString("ds_delivery_address"));
                            orderItem.setVlTotal(itemObject.getString("vl_total"));
                            itemList.add(orderItem);
                        }
                        madapter = new OrderRecyclerAdapter(itemList, EditOrder.this);
                        rcv.setLayoutManager(mlayoutmansager);
                        rcv.setAdapter(madapter);
                        madapter.SetOnItemClickListener(new OrderRecyclerAdapter.OnItemClickListener() {
                            @Override
                            public void OnItemClick(int position) {
                                Intent intent = new Intent(EditOrder.this, OrderDetails.class);
                                intent.putExtra("OrderId", itemList.get(position).getCdSalesOrder());
                                intent.putExtra("cus", itemList.get(position).getDsCustomer());
                                intent.putExtra("branch", itemList.get(position).getCdBranch());
                                startActivity(intent);
                            }
                        });
                    }
else{
                        Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

}
