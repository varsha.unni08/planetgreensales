package com.example.appleapple.sampleapp1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Collectiondetails {

    @SerializedName("cd_branch")
    @Expose
    private String cdBranch;
    @SerializedName("cd_customer")
    @Expose
    private String cdCustomer;
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer;
    @SerializedName("ds_sales_person")
    @Expose
    private String dsSalesPerson;
    @SerializedName("ds_branch_code")
    @Expose
    private String dsBranchCode;
    @SerializedName("vl_amount")
    @Expose
    private String vlAmount;
    @SerializedName("dt_record")
    @Expose
    private String dtRecord;

    public Collectiondetails() {
    }

    public String getCdBranch() {
        return cdBranch;
    }

    public void setCdBranch(String cdBranch) {
        this.cdBranch = cdBranch;
    }

    public String getCdCustomer() {
        return cdCustomer;
    }

    public void setCdCustomer(String cdCustomer) {
        this.cdCustomer = cdCustomer;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getDsSalesPerson() {
        return dsSalesPerson;
    }

    public void setDsSalesPerson(String dsSalesPerson) {
        this.dsSalesPerson = dsSalesPerson;
    }

    public String getDsBranchCode() {
        return dsBranchCode;
    }

    public void setDsBranchCode(String dsBranchCode) {
        this.dsBranchCode = dsBranchCode;
    }

    public String getVlAmount() {
        return vlAmount;
    }

    public void setVlAmount(String vlAmount) {
        this.vlAmount = vlAmount;
    }

    public String getDtRecord() {
        return dtRecord;
    }

    public void setDtRecord(String dtRecord) {
        this.dtRecord = dtRecord;
    }
}
