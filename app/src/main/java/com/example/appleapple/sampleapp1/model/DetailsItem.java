package com.example.appleapple.sampleapp1.model;

public class DetailsItem{
	private String vlBalance;
	private String dsBranch;
	private String cdBranch;

	public void setVlBalance(String vlBalance){
		this.vlBalance = vlBalance;
	}

	public String getVlBalance(){
		return vlBalance;
	}

	public void setDsBranch(String dsBranch){
		this.dsBranch = dsBranch;
	}

	public String getDsBranch(){
		return dsBranch;
	}

	public void setCdBranch(String cdBranch){
		this.cdBranch = cdBranch;
	}

	public String getCdBranch(){
		return cdBranch;
	}

	@Override
 	public String toString(){
		return 
			"DetailsItem{" + 
			"vl_balance = '" + vlBalance + '\'' + 
			",ds_branch = '" + dsBranch + '\'' + 
			",cd_branch = '" + cdBranch + '\'' + 
			"}";
		}
}
