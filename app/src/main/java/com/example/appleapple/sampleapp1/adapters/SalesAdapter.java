package com.example.appleapple.sampleapp1.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.model.Sales;

import java.util.List;

public class SalesAdapter  extends RecyclerView.Adapter<SalesAdapter.SalesHolder> {

    Context context;
    List<Sales>sales;

    public SalesAdapter(Context context, List<Sales> sales) {
        this.context = context;
        this.sales = sales;
    }

    public class SalesHolder extends RecyclerView.ViewHolder{

        TextView txtPattern;

        public SalesHolder(@NonNull View itemView) {
            super(itemView);

            txtPattern=itemView.findViewById(R.id.txtPattern);
        }
    }

    @NonNull
    @Override
    public SalesHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_stockadapter,viewGroup,false);



        return new SalesHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SalesHolder salesHolder, int i) {

        salesHolder.txtPattern.setText(sales.get(i).getDsCustomer()+"\n"+sales.get(i).getDsSalesInvoice()+"\n"+sales.get(i).getDtProductSale()+"\n"+sales.get(i).getSalesType()+"\n"+sales.get(i).getVlGrandTotal()+"\nTotal : "+sales.get(i).getVlGrandTotal());

    }

    @Override
    public int getItemCount() {
        return sales.size();
    }
}
