package com.example.appleapple.sampleapp1.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.R;

import java.util.List;

/**
 * @author Sooraj Soman on 4/14/2019
 */
public class RecyclerTarget extends RecyclerView.Adapter<RecyclerTarget.TargetViewHolder> {
    private final Context context;
    private List<TargetItem> items;

    public RecyclerTarget(List<TargetItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public TargetViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_achievements, parent, false);
        return new TargetViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TargetViewHolder holder, int position) {
        TargetItem item = items.get(position);
        holder.set(item);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class TargetViewHolder extends RecyclerView.ViewHolder {
TextView name,datev,total;
        public TargetViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.cname);
            datev = (TextView) itemView.findViewById(R.id.date);
            total = (TextView) itemView.findViewById(R.id.cTotal);
        }

        public void set(TargetItem item) {
            if(!TextUtils.isEmpty(item.getDsCustomer())){
                name.setText("Customer : "+item.getDsCustomer());
            }  if(!TextUtils.isEmpty(item.getDtDelivery())){
               datev.setText("Date : "+item.getDtDelivery());
            }if(!TextUtils.isEmpty(item.getVlTotal())){
               total.setText("Total : "+item.getVlTotal());
            }
            //UI setting code
        }
    }
}