package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.activities.HomeActivity;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {

    EditText user,pswd;
    Button btn1;
    TextView result;
    String url= Constants.base_url+"Login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user=(EditText) findViewById(R.id.name);
        pswd=(EditText) findViewById(R.id.pswd);
        result=(TextView)findViewById(R.id.textres);

        btn1=(Button) findViewById(R.id.btn1);
       SharedPreferences sharedpreferences = this.getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        //Log.v(TAG,"v"+sharedpreferences.getString(Constants.LOGINSTATE, ""));
        if (!TextUtils.isEmpty(sharedpreferences.getString("token", ""))) {


                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }



        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              // Toast.makeText(getApplicationContext(),"jhjhjh",Toast.LENGTH_LONG).show();
                if (user.getText().toString().matches("^[6-9]\\d{9}$")) {
                if (!TextUtils.isEmpty(pswd.getText().toString())) {
                    if(NetworkChangeReceiver.isConnected()){
                    showmsg();}else{
                        Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

                    }
                }else{
                    Toast.makeText(getApplicationContext(),"invalid password",Toast.LENGTH_LONG).show();

                }
                }else{
                    Toast.makeText(getApplicationContext(),"invalid mobile number",Toast.LENGTH_LONG).show();
                }





            }
        });
    }



   public void showmsg()
    {


      StringRequest sr=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject res=new JSONObject(response);
                    if(res.getString("token")!=null){

                    //result.setText(res.getString("token"));
                    Log.v("TAG","v"+res.getString("token"));

                    SharedPreferences sharedpref=getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedpref.edit();
                    editor.putString("token",res.getString("token"));
                    editor.apply();
                        startActivity(new Intent(MainActivity.this,HomeActivity.class));
                       finish();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"invalid username or password",Toast.LENGTH_LONG).show();

                        // result.setText("Invalid User id or password");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"invalid username or password",Toast.LENGTH_LONG).show();

                }


                //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(getApplicationContext(),error+"",Toast.LENGTH_SHORT).show();
                result.setText(error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params=new HashMap<String,String>();
                params.put("phone",user.getText().toString());
                params.put("password",pswd.getText().toString());
                return params;

            }
        };


        RequestQueue reque=Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);

    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            // window.setStatusBarColor(Color.TRANSPARENT);
            //window.setStatusBarColor(Color.parseColor("#e94699"));
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }
}
