package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.Branch;

import java.util.ArrayList;

public class BranchAdpater extends ArrayAdapter <Branch>{

    Context mcontext ;
    int mresource;

    public BranchAdpater(@NonNull Context context, int resource, @NonNull ArrayList<Branch> objects) {
        super(context, resource, objects);
        mcontext=context;
        mresource=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name=getItem(position).getBname();
        String stock=getItem(position).getItemQty();


        Branch prd=new Branch(stock,name);
        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView prdname=(TextView) convertView.findViewById(R.id.Txtvw_Pname);
        TextView prdQty=(TextView) convertView.findViewById(R.id.Txtvw_stock);

        prdname.setText("Branch  : "   +String.valueOf(name));
        prdQty.setText("Total Stock : "+String.valueOf(stock));
        return  convertView;


    }
}
