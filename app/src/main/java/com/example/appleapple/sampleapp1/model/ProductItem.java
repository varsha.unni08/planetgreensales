package com.example.appleapple.sampleapp1.model;

/**
 * @author Sooraj Soman on 4/13/2019
 */
public class ProductItem {
    String cd_product,ds_product;

    public ProductItem(String cd_product, String ds_product) {
        this.cd_product = cd_product;
        this.ds_product = ds_product;
    }

    public ProductItem() {
    }

    public String getCd_product() {
        return cd_product;
    }

    public void setCd_product(String cd_product) {
        this.cd_product = cd_product;
    }

    public String getDs_product() {
        return ds_product;
    }

    public void setDs_product(String ds_product) {
        this.ds_product = ds_product;
    }

    @Override
    public String toString() {
        return this.ds_product;
    }
}
