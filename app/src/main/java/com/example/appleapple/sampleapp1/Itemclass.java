package com.example.appleapple.sampleapp1;

    public class Itemclass {

        String Itemname,itemId,Qty,gst,sgst,Total,UnitPrice;

        public Itemclass(String itemname, String itemId, String qty, String gst, String sgst, String total, String unitPrice) {
            Itemname = itemname;
            this.itemId = itemId;
            Qty = qty;
            this.gst = gst;
            this.sgst = sgst;
            Total = total;
            UnitPrice = unitPrice;
        }

        public String getItemname() {
        return Itemname;
    }

    public String getQty() {
        return Qty;
    }

    public String getGst() {
        return gst;
    }

    public String getSgst() {
        return sgst;
    }

    public String getTotal() {
        return Total;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setItemname(String itemname) {
        Itemname = itemname;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }
    }
