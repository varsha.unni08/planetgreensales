package com.example.appleapple.sampleapp1.model;

public class Branch {
    String Bcode,Bname,ItemQty,balance;
    public Branch(String bcode, String bname) {
        Bcode = bcode;
        Bname = bname;
    }


    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Branch(String bcode, String bname, String itemQty) {
        Bcode = bcode;
        Bname = bname;
        ItemQty = itemQty;
    }

    public String getBcode() {
        return Bcode;
    }

    public String getBname() {
        return Bname;
    }

    @Override
    public String toString() {
        return this. Bname;
    }

    public String getItemQty() {
        return ItemQty;
    }

    public void setItemQty(String itemQty) {
        ItemQty = itemQty;
    }

}
