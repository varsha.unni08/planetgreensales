package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.adapters.ItemAdapter;
import com.example.appleapple.sampleapp1.adapters.RecyclerViewStock;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.ProductItem;
import com.example.appleapple.sampleapp1.model.Stock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewStock extends AppCompatActivity {


    StockAdpater adapter;
    RecyclerView list;
    Spinner brSpinner,spinner;
    AutoCompleteTextView autocomplete;
    ArrayList<Branch> brs = new ArrayList<Branch>();
    ArrayList<Stock> stock = new ArrayList<Stock>();
    ArrayList<ProductItem> productItems;
    RecyclerView recyclerView;
    RecyclerViewStock recyclerViewStock;
    String stockurl = Constants.base_url+"patterns.php";

    String Token;
    private ItemAdapter pAdapter;
    ProductItem selection;

    ImageView imgback;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__stock);

        getSupportActionBar().hide();
        brSpinner = (Spinner) findViewById(R.id.Br_spinner);
        imgback=findViewById(R.id.imgback);
        spinner = (Spinner) findViewById(R.id.spinner);
        autocomplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        getToken();
        // fillbranch();
        fillProductsAutocomplete();


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });




       /* brSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                @Override
                                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                    String bname = brSpinner.getSelectedItem().toString();
                                                    System.out.println(bname);
                                                    if (bname.equalsIgnoreCase("all")) {
                                                        filldata();

                                                    }
                                                    for (int pos = 0; pos < brs.size(); pos++) {
                                                        if (brs.get(pos).getBname().equalsIgnoreCase(bname)) {
                                                            String cod = brs.get(pos).getBcode();
                                                            filldata(cod);

                                                        }

                                                    }
                                                }

                                                @Override
                                                public void onNothingSelected(AdapterView<?> adapterView) {
                                                }
                                            }
        );*/
    }

   /* private void fillbranch() {
        String url = "http://www.centroidsolutions.in/6_grace/api/all_branches.php";
        StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                stock.clear();
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    ArrayList<String> pner = new ArrayList<String>();
                    pner.add("all");
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String code, name;
                        code = brObject.getString("cd_branch");
                        name = brObject.getString("ds_branch");
                        System.out.println(name + code);
                        brs.add(new Branch(code, name));
                        pner.add(name);


                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, pner);
                    brSpinner.setAdapter(adapter);


                } catch (JSONException e) {
                    adapter = null;
                    list.setAdapter(adapter);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("apicall",apicall);
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

    private void filldata() {
        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                stock.clear();
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name;
                        name = brObject.getString("ds_product");
                        stk = brObject.getString("nr_qty");
                        System.out.println(name + stk);
                        stock.add(new Stock(stk, name));


                    }
                    list = (ListView) findViewById(R.id.stocklist);
                    adapter = new StockAdpater(getApplicationContext(), R.layout.stock_list, stock);
                    list.setAdapter(adapter);


                } catch (JSONException e) {
                    adapter = new StockAdpater(getApplicationContext(), R.layout.stock_list, stock);
                    list.setAdapter(adapter);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("apicall",apicall);
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

    private void filldata(String cod) {
        adapter = null;
        stockurl = " http://www.centroidsolutions.in/6_grace/api/view_stock.php?cd_branch=" + cod;
        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                stock.clear();
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name;
                        name = brObject.getString("ds_product");
                        stk = brObject.getString("nr_qty");
                        System.out.println(name + stk);
                        stock.add(new Stock(stk, name));


                    }
                    list = (ListView) findViewById(R.id.stocklist);
                    adapter = new StockAdpater(getApplicationContext(), R.layout.stock_list, stock);
                    list.setAdapter(adapter);


                } catch (JSONException e) {
                    adapter = null;
                    list.setAdapter(adapter);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cd_branch", "1");
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }*/

    private void fillProductsAutocomplete() {
        productItems = new ArrayList<ProductItem>();

        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name;
                        name = brObject.getString("cd_pattern");
                        stk = brObject.getString("ds_pattern");
                        System.out.println(name + stk);
                        productItems.add(new ProductItem( name,stk));
                    }
                    ArrayAdapter<ProductItem> adapter = new ArrayAdapter<ProductItem>(getApplicationContext(), android.R.layout.simple_list_item_1,productItems);
                    spinner.setAdapter(adapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            selection = (ProductItem) spinner.getItemAtPosition(i);
                            getStockViews(selection);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

    private void getStockViews(ProductItem item) {
        adapter = null;
        stockurl = "http://www.centroidsolutions.in/6_grace/api/product_stock?cd_pattern=" + item.getCd_product();
        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                stock.clear();
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name, id;
                        name = brObject.getString("ds_product");
                        id = brObject.getString("cd_product");
                        stk = brObject.getString("nr_qty");
                        System.out.println(name + stk);
                        stock.add(new Stock(stk, name, id));


                    }
                    list = (RecyclerView) findViewById(R.id.stocklist);
                    recyclerViewStock = new RecyclerViewStock(stock, ViewStock.this);
                    list.setAdapter(recyclerViewStock);


                } catch (JSONException e) {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
    }
}
