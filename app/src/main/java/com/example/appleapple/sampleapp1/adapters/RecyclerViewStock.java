package com.example.appleapple.sampleapp1.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.BranchAdpater;
import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Stock;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Sooraj Soman on 4/30/2019
 */
public class RecyclerViewStock extends RecyclerView.Adapter<RecyclerViewStock.StockViewHolder> {
    private final Context context;
    private List<Stock> items;

    public RecyclerViewStock(List<Stock> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public RecyclerViewStock.StockViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stock_list, parent, false);
        return new RecyclerViewStock.StockViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewStock.StockViewHolder holder, final int position) {
        final Stock item = items.get(position);
        try {
            holder.set(item);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        postdataAsync(items.get(position), position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void postdataAsync(Stock stock, Integer pos) {
        final ArrayList<Branch> stocks = new ArrayList<>();
        SharedPreferences sharedpref = context.getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        final String Token = "Bearer " + sharedpref.getString("token", "not null");
        String stockurl = Constants.base_url+"product_stock?cd_product=" + stock.getCd_product();
        StringRequest sr = new StringRequest(Request.Method.GET, stockurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        String stk, name, id;
                        name = brObject.getString("ds_branch");
                        id = brObject.getString("cd_branch");
                        stk = brObject.getString("nr_qty");
                        System.out.println(name + stk);
                        stocks.add(new Branch(id, name, stk));


                    }
                    showDialog(stocks);

                } catch (JSONException e) {
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(context);
        reque.add(sr);
    }

    private void showDialog(ArrayList<Branch> stocks) {
        final AlertDialog.Builder mbuilder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View mview = layoutInflater.inflate(R.layout.list_stockview, null);
        mbuilder.setView(mview);
        ListView listView = (ListView) mview.findViewById(R.id.rec_view);
        mbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        BranchAdpater adapter = new BranchAdpater(context, R.layout.stock_list, stocks);
        listView.setAdapter(adapter);
        final AlertDialog dialog = mbuilder.create();
        dialog.show();
        dialog.setTitle("Branch View");
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class StockViewHolder extends RecyclerView.ViewHolder {
        TextView prdname, prdQty;

        public StockViewHolder(View itemView) {
            super(itemView);
            prdname = (TextView) itemView.findViewById(R.id.Txtvw_Pname);
            prdQty = (TextView) itemView.findViewById(R.id.Txtvw_stock);
        }

        public void set(Stock item) {
            prdname.setText("Product : " + item.getProduct());
            prdQty.setText("Stock qty : " + item.getStock());
            //UI setting code
        }
    }
}