package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.Product;

import java.util.ArrayList;

public class ProductListAdapter extends ArrayAdapter<Product> {
    Context mcontext ;
    int mresource;

    public ProductListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Product> objects) {
        super(context, resource, objects);
        mcontext=context;
        mresource=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name=getItem(position).getName();
        int total=getItem(position).getTotal();
        int qty=getItem(position).getQty();
        double rate=0.0;
    String id="";

        Product prd=new Product(id,name,total,qty,rate);
        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView prdname=(TextView) convertView.findViewById(R.id.Pname);
        TextView prdQty=(TextView) convertView.findViewById(R.id.PQty);
        TextView prdprice=(TextView) convertView.findViewById(R.id.PTotal);
        prdname.setText(name);
        prdQty.setText("Quantity"  +String.valueOf(qty));
        prdprice.setText("Total  "+String.valueOf(total));
        return  convertView;


    }
}
