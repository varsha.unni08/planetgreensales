package com.example.appleapple.sampleapp1.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.model.ProductItem;

import java.util.ArrayList;
import java.util.List;
public class ItemAdapter extends ArrayAdapter<ProductItem> {
    private Context context;
    private int resourceId;
    private List<ProductItem> items, tempItems, suggestions;
    public ItemAdapter(@NonNull Context context, int resourceId, ArrayList<ProductItem> items) {
        super(context, resourceId, items);
        this.items = items;
        this.context = context;
        this.resourceId = resourceId;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        try {
            Log.v("TAG","V");
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(resourceId, parent, false);
            }
            ProductItem productItem = getItem(position);
            TextView name = (TextView) view.findViewById(R.id.textV);
            name.setText(productItem.getCd_product());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
    @Nullable
    @Override
    public ProductItem getItem(int position) {
        return items.get(position);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @NonNull
    @Override
    public Filter getFilter() {
        return fruitFilter;
    }
    private Filter fruitFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            ProductItem productItem = (ProductItem) resultValue;
            return productItem.getDs_product();
        }
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (ProductItem productItem: tempItems) {
                    if (productItem.getDs_product().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(productItem);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<ProductItem> tempValues = (ArrayList<ProductItem>) filterResults.values;
            if (filterResults != null && filterResults.count > 0) {
                clear();
                for (ProductItem fruitObj : tempValues) {
                    add(fruitObj);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}