package com.example.appleapple.sampleapp1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sales {

    @SerializedName("sales_type")
    @Expose
    private String salesType;
    @SerializedName("series")
    @Expose
    private String series;
    @SerializedName("ds_customer")
    @Expose
    private String dsCustomer;
    @SerializedName("ds_sales_invoice")
    @Expose
    private String dsSalesInvoice;
    @SerializedName("dt_product_sale")
    @Expose
    private String dtProductSale;
    @SerializedName("vl_grand_total")
    @Expose
    private String vlGrandTotal;

    public Sales() {
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDsCustomer() {
        return dsCustomer;
    }

    public void setDsCustomer(String dsCustomer) {
        this.dsCustomer = dsCustomer;
    }

    public String getDsSalesInvoice() {
        return dsSalesInvoice;
    }

    public void setDsSalesInvoice(String dsSalesInvoice) {
        this.dsSalesInvoice = dsSalesInvoice;
    }

    public String getDtProductSale() {
        return dtProductSale;
    }

    public void setDtProductSale(String dtProductSale) {
        this.dtProductSale = dtProductSale;
    }

    public String getVlGrandTotal() {
        return vlGrandTotal;
    }

    public void setVlGrandTotal(String vlGrandTotal) {
        this.vlGrandTotal = vlGrandTotal;
    }
}
