package com.example.appleapple.sampleapp1.model;

public class Product {

    String Name, Id;
    int total, qty;
            Double rate;

    public Product(String id, String name, int total, int qty, double price) {
        Id = id;
        Name = name;
        this.total = total;
        this.qty = qty;
        rate = price;


    }

    public Product(String id, String name) {
        Id = id;
        Name = name;


    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
