package com.example.appleapple.sampleapp1.progress;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.appleapple.sampleapp1.R;


public class ProgressFragment extends DialogFragment {



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       getDialog().setTitle(R.string.app_name);
        return inflater.inflate(R.layout.layout_progress,container,false);
    }


}
