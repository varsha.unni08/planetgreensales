package com.example.appleapple.sampleapp1;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.activities.ARPActivity;
import com.example.appleapple.sampleapp1.adapters.TargetAdapter;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.RecyclerTarget;
import com.example.appleapple.sampleapp1.model.Target;
import com.example.appleapple.sampleapp1.model.TargetItem;
import com.example.appleapple.sampleapp1.progress.ProgressFragment;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TargetActivity extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {


    String Token;

    private ArrayList<TargetItem> itemList;

    ImageView imgback, imgTarget,imgfilter;

    TextView txtTargettypes;

    ProgressFragment progressFragment;

    int target = -1;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    RecyclerView rec_view;
    TextView from, to;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target);
        getSupportActionBar().hide();
        getToken();
       // from = (TextView) findViewById(R.id.from);
        rec_view=findViewById(R.id.rec_view);
       // to = (TextView) findViewById(R.id.to);
        txtTargettypes = findViewById(R.id.txtTargettypes);

        imgTarget = findViewById(R.id.imgTarget);
        imgfilter=findViewById(R.id.imgfilter);


       // search = (Button) findViewById(R.id.search);
        imgback = findViewById(R.id.imgback);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        txtTargettypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTargetList();

            }
        });

        imgTarget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTargetList();
            }
        });

        imgfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Button search;

               final Dialog dialog=new Dialog(TargetActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.layout_targetdate);

                from = (TextView) dialog.findViewById(R.id.from);

                to = (TextView)dialog. findViewById(R.id.to);

                search = (Button)dialog. findViewById(R.id.search);


                DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int widthLcl = (int) (displayMetrics.widthPixels);
                int heightLcl = (int)getResources().getDimension(R.dimen.dimen300dp);

                dialog.getWindow().setLayout(widthLcl,heightLcl);



                from.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDatePickerDialog(1);
                    }
                });
                to.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDatePickerDialog(2);
                    }
                });
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(from.getText().toString())) {
                            if (!TextUtils.isEmpty(to.getText().toString())) {

                                if (target != -1) {

                                    dialog.dismiss();

                                    if (NetworkChangeReceiver.isConnected()) {

                                        String url = Constants.base_url+"get_targets.php?type=" + target + "&from=" + from.getText().toString() + "&to=" + to.getText().toString();



                                        getTarget(url);
                                    }

                                    else {
                                        Toast.makeText(getApplicationContext(), "Please check internet connection ", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please select target ", Toast.LENGTH_SHORT).show();

                                }



                            }

                            else {
                                Toast.makeText(getApplicationContext(), "Please select to date ", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Please select from date ", Toast.LENGTH_SHORT).show();

                        }
                    }
                });


                dialog.show();
            }
        });

        //getdata();



//        if (!TextUtils.isEmpty(from.getText().toString())) {
//            if (!TextUtils.isEmpty(to.getText().toString())) {
//                if (NetworkChangeReceiver.isConnected()) {
//                    getTarget(from.getText().toString(), to.getText().toString());
//                }
//            } else {
//                getTarget();
//            }
//        }
//
//        else {
//            getTarget();
//        }

    }


    public void showTargetList() {



        final String targets[] = {"sales", "collection"};
        AlertDialog.Builder builder = new AlertDialog.Builder(TargetActivity.this);
        builder.setTitle("Choose a target");
        ;


        builder.setItems(targets, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtTargettypes.setText(targets[which]);

                target = which + 1;

                String url=Constants.base_url+"get_targets.php?type="+target;

                getTarget(url);



            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDatePickerDialog(final int i) {
        try {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                            Calendar cal = Calendar.getInstance();
                            cal.set(year, monthOfYear, dayOfMonth);
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String fDate = df.format(cal.getTime());
                            if (!TextUtils.isEmpty(fDate)) {
                                if (i == 1) {
                                    from.setText(fDate);
                                } else {
                                    to.setText(fDate);
                                }
                            }
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } catch (Exception e) {
        }
    }



    private void getTarget(String url) {
               StringRequest sr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.println(response);
                    JSONArray res = new JSONArray(response);
                    //getting the json object of the particular index inside the array
//                    JSONObject brObject = res.getJSONObject(0);
//                    System.out.println(brObject);

                    List<Target>targets=new ArrayList<>();

                    for (int i=0;i<res.length();i++)
                    {
                        JSONObject brObject = res.getJSONObject(i);

                        Target target=new GsonBuilder().create().fromJson(brObject.toString(),Target.class);
                        targets.add(target);
                    }

                    if(targets.size()>0)
                    {

                        rec_view.setVisibility(View.VISIBLE);
                        rec_view.setLayoutManager(new LinearLayoutManager(TargetActivity.this));
                        rec_view.setAdapter(new TargetAdapter(TargetActivity.this,targets));
                    }
                    else {

                        rec_view.setVisibility(View.GONE);

                        Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();


                    }





                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                    //Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());

                rec_view.setVisibility(View.GONE);

                Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }

//    private void getdata() {
//        String Cr_Url = "http://www.centroidsolutions.in/6_grace/api/view_orders.php?apicall=achievement";
//        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    //getting the json object of the particular index inside the array
//
//                    JSONArray jsonArray = new JSONArray(response);
//                    itemList = new ArrayList<>();
//                    if (jsonArray != null) {
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject itemObject = jsonArray.getJSONObject(i);
//                            TargetItem targetItem = new TargetItem();
//
//                            targetItem.setCdSalesOrder(itemObject.getString("cd_sales_order"));
//                            targetItem.setCdBranch(itemObject.getString("cd_branch"));
//                            targetItem.setDtDelivery(itemObject.getString("dt_delivery"));
//                            targetItem.setDsDeliveryAddress(itemObject.getString("ds_delivery_address"));
//                            targetItem.setVlTotal(itemObject.getString("vl_total"));
//                            targetItem.setDsCustomer(itemObject.getString("ds_customer"));
//                            targetItem.setCdCustomer(itemObject.getString("cd_customer"));
//                            itemList.add(targetItem);
//                        }
//                        RecyclerView list = (RecyclerView) findViewById(R.id.rec_view);
//                        RecyclerTarget adapter = new RecyclerTarget(itemList, TargetActivity.this);
//                        list.setAdapter(adapter);
//                    } else {
//
//                        Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getApplicationContext(), "No data  present! ", Toast.LENGTH_SHORT).show();
//
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                System.out.println(error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Authorization", Token);
//                return headers;
//            }
//
//
//        };
//        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
//        reque.add(sr);
//
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }
}
