package com.example.appleapple.sampleapp1.model;

public class Stock {

    String Stock,product,cd_product;

    public Stock(String stock, String product) {
        Stock = stock;
        this.product = product;
    }

    public Stock(String stock, String product, String cd_product) {
        Stock = stock;
        this.product = product;
        this.cd_product = cd_product;
    }

    public String getStock() {
        return Stock;
    }

    public void setStock(String stock) {
        Stock = stock;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCd_product() {
        return cd_product;
    }

    public void setCd_product(String cd_product) {
        this.cd_product = cd_product;
    }
}
