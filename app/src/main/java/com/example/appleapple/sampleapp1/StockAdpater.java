package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.Stock;

import java.util.ArrayList;

public class StockAdpater extends ArrayAdapter <Stock>{

    Context mcontext ;
    int mresource;

    public StockAdpater(@NonNull Context context, int resource, @NonNull ArrayList<Stock> objects) {
        super(context, resource, objects);
        mcontext=context;
        mresource=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name=getItem(position).getProduct();
        String stock=getItem(position).getStock();


        Stock prd=new Stock(stock,name);
        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView prdname=(TextView) convertView.findViewById(R.id.Txtvw_Pname);
        TextView prdQty=(TextView) convertView.findViewById(R.id.Txtvw_stock);

        prdname.setText("Product :"  +String.valueOf(name));
        prdQty.setText("Stock :"+String.valueOf(stock));
        return  convertView;


    }
}
