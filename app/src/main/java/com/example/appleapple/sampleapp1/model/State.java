
package com.example.appleapple.sampleapp1.model;


public class State {

    private String cdState;
    private String dsState;

    /**
     * No args constructor for use in serialization
     */
    public State() {
    }

    /**
     * @param dsState
     * @param cdState
     */
    public State(String cdState, String dsState) {
        super();
        this.cdState = cdState;
        this.dsState = dsState;
    }

    public String getCdState() {
        return cdState;
    }

    public void setCdState(String cdState) {
        this.cdState = cdState;
    }

    public String getDsState() {
        return dsState;
    }

    public void setDsState(String dsState) {
        this.dsState = dsState;
    }

    @Override
    public String toString() {
        return this. dsState;
    }
}