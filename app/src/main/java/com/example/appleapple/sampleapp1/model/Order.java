package com.example.appleapple.sampleapp1.model;

public class Order {
    String OrderId, Customername;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getCustomername() {
        return Customername;
    }

    public void setCustomername(String customername) {
        Customername = customername;
    }

    public Order(String orderId, String customername) {
        OrderId = orderId;
        Customername = customername;
    }


}
