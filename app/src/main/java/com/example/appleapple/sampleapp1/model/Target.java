package com.example.appleapple.sampleapp1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Target {

    @SerializedName("vl_target_in_rupees")
    @Expose
    private String vlTargetInRupees;
    @SerializedName("ds_target_in_sqt")
    @Expose
    private String dsTargetInSqt;
    @SerializedName("vl_achievement_in_rupees")
    @Expose
    private String vlAchievementInRupees;

    public Target() {
    }

    public String getVlTargetInRupees() {
        return vlTargetInRupees;
    }

    public void setVlTargetInRupees(String vlTargetInRupees) {
        this.vlTargetInRupees = vlTargetInRupees;
    }

    public String getDsTargetInSqt() {
        return dsTargetInSqt;
    }

    public void setDsTargetInSqt(String dsTargetInSqt) {
        this.dsTargetInSqt = dsTargetInSqt;
    }

    public String getVlAchievementInRupees() {
        return vlAchievementInRupees;
    }

    public void setVlAchievementInRupees(String vlAchievementInRupees) {
        this.vlAchievementInRupees = vlAchievementInRupees;
    }
}
