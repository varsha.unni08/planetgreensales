package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.model.Branch;

import com.example.appleapple.sampleapp1.model.Response;

import java.util.ArrayList;

public class CustomerAdpater extends ArrayAdapter <Response>{

    Context mcontext ;
    int mresource;

    public CustomerAdpater(@NonNull Context context, int resource, @NonNull ArrayList<Response> objects) {
        super(context, resource, objects);
        mcontext=context;
        mresource=resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name=getItem(position).getDsCustomer();
        String stock=getItem(position).getVlBalance();


        Branch prd=new Branch(stock,name);
        LayoutInflater inflater=LayoutInflater.from(mcontext);
        convertView=inflater.inflate(mresource,parent,false);
        TextView prdname=(TextView) convertView.findViewById(R.id.Txtvw_Pname);
        TextView prdQty=(TextView) convertView.findViewById(R.id.Txtvw_stock);

        prdname.setText("Customer  : "   +String.valueOf(name));
        prdQty.setText("Total Balance : "+String.valueOf(stock));
        return  convertView;


    }
}
