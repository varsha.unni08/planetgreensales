package com.example.appleapple.sampleapp1.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.model.Collectiondetails;

import java.util.List;

public class CollectionAdapter  extends RecyclerView.Adapter<CollectionAdapter.CollectionHolder> {

    Context context;
    List<Collectiondetails>collectiondetails;

    public CollectionAdapter(Context context, List<Collectiondetails> collectiondetails) {
        this.context = context;
        this.collectiondetails = collectiondetails;
    }

    public class CollectionHolder extends RecyclerView.ViewHolder{
        TextView txtPattern;
        public CollectionHolder(@NonNull View itemView) {
            super(itemView);

            txtPattern=itemView.findViewById(R.id.txtPattern);
        }
    }

    @NonNull
    @Override
    public CollectionHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(context).inflate(R.layout.layout_stockadapter,viewGroup,false);



        return new CollectionHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionHolder collectionHolder, int i) {

        collectionHolder.txtPattern.setText(collectiondetails.get(i).getDsCustomer()+"\n"+collectiondetails.get(i).getDsBranchCode()+"\n"+collectiondetails.get(i).getDtRecord()+"\nAmount : "+collectiondetails.get(i).getVlAmount());


    }

    @Override
    public int getItemCount() {
        return collectiondetails.size();
    }
}
