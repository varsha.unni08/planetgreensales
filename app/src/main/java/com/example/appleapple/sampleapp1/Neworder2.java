package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.activities.HomeActivity;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Product;
import com.example.appleapple.sampleapp1.model.ProductItem;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Neworder2 extends AppCompatActivity {

    Button chsitem, order_btn, can_button;
    ListView list;
    String Pr_Url;
    String Branch, C_name, CAddress, Del_date;
    ArrayList<Product> prds = new ArrayList<Product>();
    int Totalamt = 0;
    String stock, upr;
    SearchableSpinner pname;

    Integer cgst = null;
    Integer sgst = null;
    Integer stockQty = null;
    ArrayList<String> PrdnamesArray = new ArrayList<String>();

    String Token;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    public void getData() {
        Intent intent = getIntent();
        Branch = intent.getStringExtra("Brname");
        C_name = intent.getStringExtra("Cname");
        CAddress = intent.getStringExtra("DAddress");
        Del_date = intent.getStringExtra("DDate");
        Log.v("TAG","d"+Del_date);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neworder2);
        chsitem = (Button) findViewById(R.id.chsitem);
        list = (ListView) findViewById(R.id.Orderlist);
        order_btn = (Button) findViewById(R.id.OrderButton);
        can_button = (Button) findViewById(R.id.CancelButton);
        can_button.setOnClickListener(new OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              startActivity(new Intent(Neworder2.this, HomeActivity.class));
                                          }
                                      }
        );
        getToken();
        getData();
        updateList();
        order_btn.setOnClickListener(new OnClickListener() {
                                         @Override
                                         public void onClick(View view) {

                                             if(prds!=null&&prds.size()>0) {
                                                 placeOrder();
                                             }else{
                                                 Toast.makeText(getApplicationContext(), "please add item ", Toast.LENGTH_SHORT).show();

                                             }
                                         }
                                     }
        );

        chsitem.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           final AlertDialog.Builder mbuilder = new AlertDialog.Builder(Neworder2.this);
                                           final View mview = getLayoutInflater().inflate(R.layout.chooseitem_dialog, null);
                                           mbuilder.setView(mview);

                                           pname = (SearchableSpinner) mview.findViewById(R.id.act_prd);
                                           fillProduct();
                                           mbuilder.setTitle("Choose Item");
                                           final EditText uprice = (EditText) mview.findViewById(R.id.tv_unitPrice);
                                           final  EditText Total = (EditText) mview.findViewById(R.id.txt_PTotal);
                                           final EditText Tqty = (EditText) mview.findViewById(R.id.txt_qty);
                                           Tqty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                               @Override
                                               public void onFocusChange(View view, boolean b) {
                                                   try {
                                                       int qty = Integer.parseInt(Tqty.getText().toString());
                                                       int upr = Integer.parseInt(uprice.getText().toString());
                                                         if(upr!=0){
                                                       Integer totVal = qty * upr;
                                                       double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                                       Total.setText(String.valueOf((int) currentTotl));}else{
                                                           Toast.makeText(getApplicationContext(),"Please enter unit price",Toast.LENGTH_SHORT).show();
                                                       }

                                                   } catch (Exception ex) {

                                                   }


                                               }
                                           });
                                        uprice.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                                try {

                                                    if(!TextUtils.isEmpty(Tqty.getText().toString())){
                                                    int qty = Integer.parseInt(Tqty.getText().toString());

                                                    double upr=(Double.parseDouble(uprice.getText().toString()));

                                                    if(upr>0.0){
                                                        Integer totVal = (int)(qty * upr);
                                                        double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                                        Total.setText(String.valueOf((int) currentTotl));}else{
                                                        Toast.makeText(getApplicationContext(),"Please enter unit price",Toast.LENGTH_SHORT).show();
                                                    }}

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                        Tqty.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                                try {

                                                    if(!TextUtils.isEmpty(Tqty.getText().toString())){
                                                    int qty = Integer.parseInt(Tqty.getText().toString());

                                                    double upr=(Double.parseDouble(uprice.getText().toString()));

                                                    if(upr>0.0){
                                                        Integer totVal = (int)(qty * upr);
                                                        double currentTotl = totVal + (totVal * (cgst * .01)) + (totVal * (sgst * .01));
                                                        Total.setText(String.valueOf((int) currentTotl));}else{
                                                        Toast.makeText(getApplicationContext(),"Please enter unit price",Toast.LENGTH_SHORT).show();
                                                    }}

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                           /*uprice.set(new View.OnFocusChangeListener() {
                                               @Override
                                               public void onFocusChange(View view, boolean b) {



                                               }
                                           });*/
                                           pname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                               @Override
                                               public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                   String item = ((ProductItem) pname.getSelectedItem()).getCd_product();
                                                   //****************
                                                   String Cr_Url = Constants.base_url+"product_details.php?cd_product=" + item + "&cd_branch=" + Branch;
                                                   System.out.println(Cr_Url);
                                                   StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
                                                       @Override
                                                       public void onResponse(String response) {
                                                           try {
                                                               JSONArray res = new JSONArray(response);
                                                               //getting the json object of the particular index inside the array
                                                               JSONObject brObject = res.getJSONObject(0);
                                                               //String Product = brObject.getString("ds_product");
                                                               String Uprice = brObject.getString("vl_sell_price");
                                                               String stock = brObject.getString("stock");
                                                               TextView stockview = mview.findViewById(R.id.tv_stock);
                                                               TextView uprice = mview.findViewById(R.id.tv_unitPrice);
                                                               TextView Tv_pname = (TextView) mview.findViewById(R.id.pname);
                                                               //Tv_pname.setText(Product);
                                                               sgst = brObject.getInt("vl_sgst");
                                                               cgst = brObject.getInt("vl_cgst");
                                                               stockview.setText(stock);
                                                               uprice.setText(Uprice);
                                                               System.out.println(brObject.toString());


                                                           } catch (JSONException e) {
                                                               e.printStackTrace();
                                                           }


                                                       }
                                                   }, new Response.ErrorListener() {
                                                       @Override
                                                       public void onErrorResponse(VolleyError error) {
                                                           TextView txtres = (TextView) findViewById(R.id.Res);
                                                           txtres.setText(error.toString());
                                                       }
                                                   }) {
                                                       @Override
                                                       public Map<String, String> getHeaders() throws AuthFailureError {
                                                           HashMap<String, String> headers = new HashMap<String, String>();
                                                           headers.put("Authorization", Token);
                                                           return headers;
                                                       }


                                                   };
                                                   RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
                                                   reque.add(sr);
                                                   //*****************
                                               }

                                               @Override
                                               public void onNothingSelected(AdapterView<?> adapterView) {
                                               }
                                           });
                                           Button btn_cancel = (Button) mview.findViewById(R.id.can_prd);

//**********************************************************************************************************************************
                                           Button btnprd = (Button) mview.findViewById(R.id.btn_add_prd);

                                           mbuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                               @Override
                                               public void onClick(DialogInterface dialogInterface, int i) {
                                               }
                                           });
                                           mbuilder.setPositiveButton("Add Product", new DialogInterface.OnClickListener() {
                                               @Override
                                               public void onClick(DialogInterface dialogInterface, int i) {


                                                   JSONObject objorderdata = new JSONObject();
                                                   EditText Tqty = (EditText) mview.findViewById(R.id.txt_qty);
                                                   Spinner prd = (Spinner) mview.findViewById(R.id.act_prd);
                                                   EditText Total = (EditText) mview.findViewById(R.id.txt_PTotal);
                                                   TextView Tv_Stock = (TextView) mview.findViewById(R.id.tv_stock);
                                                   TextView Tv_UnitPrice = (TextView) mview.findViewById(R.id.tv_unitPrice);
                                                   TextView Tv_pname = (TextView) mview.findViewById(R.id.pname);
                                                   int stock, qnty;
                                                   stock = Integer.parseInt(Tv_Stock.getText().toString());
                                                   qnty = Integer.parseInt(Tqty.getText().toString());
                                               /*    if (qnty > stock) {
                                                       Toast.makeText(getApplicationContext(), "Less Stock available", Toast.LENGTH_LONG);
                                                       Tqty.setText("0");
                                                   } else {*/
                                                       String pid = ((ProductItem) prd.getSelectedItem()).getCd_product();
                                                       String pname = Tv_pname.getText().toString();
                                                       String qty = Tqty.getText().toString();
                                                       String pTotal = Total.getText().toString();
                                                       String Uprice = Tv_UnitPrice.getText().toString();
                                                        double upr=Double.parseDouble(uprice.getText().toString());
                                                       Product selPrd = new Product(pid, pname, Integer.parseInt(pTotal), Integer.parseInt(qty),upr);
                                                       prds.add(selPrd);
                                                       Totalamt = Totalamt + Integer.parseInt(pTotal);
                                                       updateList();
                                                       updateTotal(Totalamt);
                                                       dialogInterface.dismiss();
                                                  // }
                                           }
                                           });
                                           final AlertDialog dialog = mbuilder.create();
                                           dialog.show();
                                           dialog.setTitle("Choose item");
                                           WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                           lp.copyFrom(dialog.getWindow().getAttributes());
                                           lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                                           lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                           dialog.getWindow().setAttributes(lp);


                                       }
                                   }
        );
    }

    private void updateTotal(int totalamt) {
        TextView total = (TextView) findViewById(R.id.PTotal);
        total.setText("Total  " + String.valueOf(Totalamt));

    }


    public void placeOrder() {
        JSONObject objOrderdata = new JSONObject();
        JSONObject obj;
        JSONArray orderedItems = new JSONArray();
        try {
            objOrderdata.put("cd_branch", Branch);
            objOrderdata.put("cd_customer", C_name);
            objOrderdata.put("ds_delivery_address", CAddress);
            objOrderdata.put("dt_delivery", Del_date);
            objOrderdata.put("grand_total", Totalamt);
            for (int i = 0; i < prds.size(); i++) {
                Product p = prds.get(i);
                obj = new JSONObject();
                obj.put("cd_product", p.getId());
                obj.put("nr_qty", p.getQty());
                obj.put("vl_total", p.getTotal());
                orderedItems.put(i, obj);


            }
            objOrderdata.put("saleitems", orderedItems);
            System.out.println(objOrderdata.toString());


        } catch (JSONException e) {
            System.out.println("error");
            e.printStackTrace();
        }
        //**************************************************************
//voley code begins
// **************************************************************
        String Cr_Url = Constants.base_url+"new_sales_order.php";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                com.android.volley.Request.Method.POST, Cr_Url,
                objOrderdata,
                new com.android.volley.Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        String orderNo = "0";
                        System.out.println("*********************************");
                        System.out.println("reponse" + response);
                        try {
                            orderNo = response.getString("cd_sales_order");
                            Toast.makeText(getApplicationContext(), "Order palced Successfully Order Id " + orderNo, Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Neworder2.this, HomeActivity.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }


        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(jsonObjectRequest);


    }

    public void fillProduct() {
        PrdnamesArray.clear();
        //  PrdnamesArray.add("Product1");
        // PrdnamesArray.add("Product3");
        // PrdnamesArray.add("Product5");
        //**************************************************
        String Cr_Url = Constants.base_url+"all_products.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    List<ProductItem> list = new ArrayList<>();
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        System.out.println(brObject.getString("cd_product"));
                        list.add(new ProductItem(brObject.getString("cd_product"), brObject.getString("ds_product")));

                    }
                    ArrayAdapter<ProductItem> prdAdapter = new ArrayAdapter<ProductItem>(Neworder2.this, android.R.layout.simple_spinner_item, list);
                    pname.setAdapter(prdAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        //************************************************
    }

    public void updateList() {
        ProductListAdapter listadaper = new ProductListAdapter(this, R.layout.product_list_layout, prds);
        //ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,prds);
        list.setAdapter(listadaper);
    }

}
