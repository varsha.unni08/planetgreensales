package com.example.appleapple.sampleapp1.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.R;
import com.example.appleapple.sampleapp1.adapters.CollectionAdapter;
import com.example.appleapple.sampleapp1.adapters.SalesAdapter;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Collectiondetails;
import com.example.appleapple.sampleapp1.model.Sales;
import com.example.appleapple.sampleapp1.progress.ProgressFragment;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionsActivity extends AppCompatActivity {

    ImageView imgback;

    ImageView imgselectBranch;

    TextView txtBranchSelection,txtTotalCount;

    RecyclerView rv;

    String Token="",branchid="";

    List<Branch> spinnerArray_br;

    Button btnSubmit;

    TextView txtSalesperson, txtStartdate, txttoDate;

    AppCompatImageView imgpickFromDate, imgpickToDate;;


    String start_date="",end_date="";

    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collections);
        getToken();
        getSupportActionBar().hide();

        spinnerArray_br=new ArrayList<>();
        imgback=findViewById(R.id.imgback);

        imgselectBranch=findViewById(R.id.imgselectBranch);
        txtBranchSelection=findViewById(R.id.txtBranchSelection);

        txtTotalCount=findViewById(R.id.txtTotalCount);

        rv=findViewById(R.id.rv);

        imgpickFromDate =findViewById(R.id.imgpickFromDate);
        imgpickToDate = findViewById(R.id.imgpickToDate);


        txtStartdate =findViewById(R.id.txtStartdate);
        txttoDate = findViewById(R.id.txttoDate);

        btnSubmit=findViewById(R.id.btnSubmit);


        imgselectBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fillBranch();

            }
        });

        txtBranchSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fillBranch();

            }
        });

        imgpickToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        imgpickFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker(1);

            }
        });

        txtStartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(1);

            }
        });
        txttoDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(0);

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!branchid.equals(""))
                {
                    if(!start_date.equals(""))
                    {
                        if(!end_date.equals(""))
                        {


                            //showSalesDetails();

                            showCollectionDetails();
                        }
                        else {

                            Toast.makeText(CollectionsActivity.this,"Select end date",Toast.LENGTH_SHORT).show();
                        }

                    }
                    else {

                        Toast.makeText(CollectionsActivity.this,"Select start date",Toast.LENGTH_SHORT).show();
                    }

                }
                else {

                    Toast.makeText(CollectionsActivity.this,"Select branch",Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }

    public void fillBranch() {



        final String Br_Url = Constants.base_url+"all_branches.php";

        if(spinnerArray_br.size()==0) {

            progressFragment=new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(),"dfjk");


            StringRequest sr = new StringRequest(Request.Method.GET, Br_Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressFragment.dismiss();
                    try {

                        JSONArray res = new JSONArray(response);
                        for (int i = 0; i < res.length(); i++) {
                            //getting the json object of the particular index inside the array
                            JSONObject brObject = res.getJSONObject(i);
                            // Log.d("Branch",brObject.getString("cd_branch"));
                            spinnerArray_br.add(new Branch(brObject.getString("cd_branch"), brObject.getString("ds_branch")));

                        }
//                    ArrayAdapter<Branch> adapter = new ArrayAdapter<Branch>(
//                            Neworder1.this, R.layout.spinner_item, spinnerArray_br);
//                    adapter.setDropDownViewResource(R.layout.spinner_item);
//                    Spinner sItems = (Spinner) findViewById(R.id.br_spinner);
//                    sItems.setAdapter(adapter);

                        spinnerArray_br.add(0, new Branch("0", "All branches"));

                        showBranchList(spinnerArray_br);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressFragment.dismiss();
                    Toast.makeText(CollectionsActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization", Token);
                    return headers;
                }
            };
            RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
            reque.add(sr);
        }
        else {
            showBranchList(spinnerArray_br);
        }
    }

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }


    public void showDatePicker(final int start) {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(CollectionsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                // txtDob.setText();

                int m = i1 + 1;

                if (start == 1) {
                    start_date = i + "-" + m + "-" + i2;
                    txtStartdate.setText(start_date);
                } else {
                    end_date = i + "-" + m + "-" + i2;
                    txttoDate.setText(end_date);
                }


            }
        }, year, month, day);

        datePickerDialog.show();
    }


    public void showBranchList(final List<Branch> branches)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(CollectionsActivity.this);
        builder.setTitle("Choose a student");
        ;

        for (Branch br:branches
        ) {

            strings.add(br.getBname());

        }



        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtBranchSelection.setText(branches.get(which).getBname());

                branchid=branches.get(which).getBcode();



                // showLessonPlanlist();

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    public void showCollectionDetails()
    {
        final String Br_Url = Constants.base_url+"collection_details.php?from="+start_date+"&to="+end_date+"&cd_branch="+branchid;

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"dfjk");


        StringRequest sr = new StringRequest(Request.Method.GET, Br_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {


                    progressFragment.dismiss();

                    if(response!=null) {

                        if(!response.equals("null"))
                        {

                        List<Collectiondetails> collectiondetails = new ArrayList<>();
                        JSONArray res = new JSONArray(response);
                        for (int i = 0; i < res.length(); i++) {

                            JSONObject brObject = res.getJSONObject(i);

                            collectiondetails.add(new GsonBuilder().create().fromJson(brObject.toString(), Collectiondetails.class));


                        }

                        if (collectiondetails.size() > 0) {

                            rv.setVisibility(View.VISIBLE);
                            CollectionAdapter salesAdapter = new CollectionAdapter(CollectionsActivity.this, collectiondetails);
                            rv.setLayoutManager(new LinearLayoutManager(CollectionsActivity.this));
                            rv.setAdapter(salesAdapter);
                        } else {

                            rv.setVisibility(View.GONE);
                            Toast.makeText(CollectionsActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                        }


                        }

                        else {

                            rv.setVisibility(View.GONE);
                            Toast.makeText(CollectionsActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    else {

                        rv.setVisibility(View.GONE);
                        Toast.makeText(CollectionsActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                    TextView txtres = (TextView) findViewById(R.id.Res);
//                    txtres.setText(error.toString());

                progressFragment.dismiss();

                Toast.makeText(CollectionsActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);

    }
}
