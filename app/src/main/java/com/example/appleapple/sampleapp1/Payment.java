package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.appleapple.sampleapp1.Application.MyApplication;
import com.example.appleapple.sampleapp1.activities.HomeActivity;
import com.example.appleapple.sampleapp1.literals.Constants;
import com.example.appleapple.sampleapp1.model.Branch;
import com.example.appleapple.sampleapp1.model.Customer;
import com.example.appleapple.sampleapp1.model.OrderItem;
import com.example.appleapple.sampleapp1.reciever.NetworkChangeReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Payment extends AppCompatActivity implements NetworkChangeReceiver.ConnectivityReceiverListener {

    ArrayList<String> pmodes = new ArrayList<String>();

    ArrayList<OrderItem> orderidList;

    AutoCompleteTextView txtsearch;
    EditText amount, Txtdes;
    AutoCompleteTextView customer;

    ImageView imgback;
    com.toptoche.searchablespinnerlibrary.SearchableSpinner spinnerBranch;

    String Token;
    private OrderItem selection;
    private Customer selection1;
    private ArrayAdapter<Customer> custAdapter;
    private Branch selectionb;

    public void getToken() {
        SharedPreferences sharedpref = getSharedPreferences("tokeninfo", Context.MODE_PRIVATE);
        Token = "Bearer " + sharedpref.getString("token", "not null");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        getSupportActionBar().hide();
        getToken();
        amount = (EditText) findViewById(R.id.Txtamnt);
        imgback=findViewById(R.id.imgback);
        Txtdes = (EditText) findViewById(R.id.Txtdes);
        customer = (AutoCompleteTextView) findViewById(R.id.customer);
        Button clear= (Button) findViewById(R.id.BtnSearch);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customer.setText("");
            }
        });
        pmodes.add("Cash");
        pmodes.add("Bank");
        final Spinner pmodespinner = (Spinner) findViewById(R.id.pmode);
        spinnerBranch = (com.toptoche.searchablespinnerlibrary.SearchableSpinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, pmodes);
        pmodespinner.setAdapter(adapter);
        Button btnMkPay = (Button) findViewById(R.id.Mkpaybtn);
        if(NetworkChangeReceiver.isConnected()) {
            fillBranch();
            fillcustomer();
            btnMkPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (spinnerBranch.getSelectedItem() != null) {
                        selectionb = (Branch) spinnerBranch.getSelectedItem();
                    }
                    String pmode = pmodespinner.getSelectedItem().toString();
                    String amnt = amount.getText().toString();
                    String des = Txtdes.getText().toString();
                    if (selectionb != null && !TextUtils.isEmpty(selectionb.getBcode())) {
                        if (selection1 != null && !TextUtils.isEmpty(selection1.getCdCustomer())) {
                            if (!TextUtils.isEmpty(pmode)) {
                                if (!TextUtils.isEmpty(amnt)) {
                                    if (!TextUtils.isEmpty(des)) {
                                        MakePayment(selectionb.getBcode(), selection1.getCdCustomer(), pmode, amnt, des);
                                    } else {
                                        Toast.makeText(Payment.this, " Enter a valid description", Toast.LENGTH_LONG).show();

                                    }
                                } else {
                                    Toast.makeText(Payment.this, "Enter a valid payment amount", Toast.LENGTH_LONG).show();

                                }
                            } else {
                                Toast.makeText(Payment.this, "Select enter a valid payment mode", Toast.LENGTH_LONG).show();


                            }
                        } else {
                            Toast.makeText(Payment.this, "Select enter a valid customer", Toast.LENGTH_LONG).show();


                        }
                    } else {
                        Toast.makeText(Payment.this, "Select enter a valid branch", Toast.LENGTH_LONG).show();


                    }
                }


            });

        }else{
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    private void MakePayment(final String bid, final String cid, final String pmode, final String amnt, final String des) {
        String url = Constants.base_url+"collect_payment.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        // Log.d("Branch",brObject.getString("cd_branch"));

                        if(brObject.getString("status").equalsIgnoreCase("1")){
                            Toast.makeText(Payment.this, " Payment sucessfull", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            finish();

                        }else{
                            Toast.makeText(Payment.this, " Some thing went wrong please try again", Toast.LENGTH_LONG).show();

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(getApplicationContext(),error+"",Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cd_customer", cid);
                params.put("cd_branch", bid);
                params.put("payment_mode", pmode);
                params.put("amount", amnt);
                params.put("description", des);
                return params;

            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);


    }



   /* private void FillOrder() {
        orderidList = new ArrayList<OrderItem>();
        orderidList.clear();
        String Cr_Url = "http://www.centroidsolutions.in/6_grace/api/view_orders.php?apicall=placed";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject itemObject = res.getJSONObject(i);
                        OrderItem orderItem = new OrderItem();
                        orderItem.setCdSalesOrder(itemObject.getString("cd_sales_order"));
                        orderItem.setCdBranch(itemObject.getString("cd_branch"));
                        orderItem.setDtDelivery(itemObject.getString("dt_delivery"));
                        orderItem.setDsCustomer(itemObject.getString("ds_customer"));
                        orderItem.setCdCustomer(itemObject.getString("cd_customer"));
                        orderItem.setDsDeliveryAddress(itemObject.getString("ds_delivery_address"));
                        orderItem.setVlTotal(itemObject.getString("vl_total"));
                        orderidList.add(orderItem);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
        ArrayAdapter<OrderItem> oadapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, orderidList);
        txtsearch.setAdapter(oadapter);
        txtsearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection = (OrderItem) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
            }
        });


    }
*/

    public void fillcustomer() {
        final ArrayList<Customer> custArray = new ArrayList<>();
        // custArray.add("Anu");
        //  custArray.add("sunil");
        String Cr_Url = Constants.base_url+"get_customer.php";
        StringRequest sr = new StringRequest(Request.Method.GET, Cr_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        Customer customer = new Customer();
                        customer.setCdCustomer(brObject.getString("cd_customer"));
                        customer.setDsCustomer(brObject.getString("ds_customer"));
                        customer.setDsPhoneNo(brObject.getString("ds_phone_no"));
                        customer.setDsEmail(brObject.getString("ds_email"));
                        customer.setDsHouseName(brObject.getString("ds_house_name"));
                        customer.setDsStreetName(brObject.getString("ds_street_name"));
                        customer.setDsLocation(brObject.getString("ds_location"));
                        customer.setDsLocation(brObject.getString("ds_tin_number"));
                        Log.d("Customer", brObject.getString("ds_customer"));
                        custArray.add(customer);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
//********************************************************
        custAdapter = new ArrayAdapter<Customer>(this, android.R.layout.simple_list_item_1, custArray);
        customer.setAdapter(custAdapter);
        customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection1 = (Customer) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
            }
        });

    }

    public void fillBranch() {
        final String Br_Url = Constants.base_url+"all_branches.php";
        final ArrayList<Branch> branchList = new ArrayList<Branch>();
        // spinnerArray_br.add("Branch1");
        //spinnerArray_br.add("Branch2");
        StringRequest sr = new StringRequest(Request.Method.GET, Br_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("kj", "kjkjkj");
                    JSONArray res = new JSONArray(response);
                    for (int i = 0; i < res.length(); i++) {
                        //getting the json object of the particular index inside the array
                        JSONObject brObject = res.getJSONObject(i);
                        // Log.d("Branch",brObject.getString("cd_branch"));
                        branchList.add(new Branch(brObject.getString("cd_branch"), brObject.getString("ds_branch")));

                    }
                    ArrayAdapter<Branch> adapter = new ArrayAdapter<Branch>(
                            Payment.this, R.layout.spinner_item, branchList);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    spinnerBranch.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                TextView txtres = (TextView) findViewById(R.id.Res);
//                txtres.setText(error.toString());

                Toast.makeText(Payment.this,error.toString(),Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        RequestQueue reque = Volley.newRequestQueue(getApplicationContext());
        reque.add(sr);
    }
    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {


        } else {
            Snackbar.make(findViewById(android.R.id.content), "No internet connection.Please try after some time!", Snackbar.LENGTH_SHORT).show();

        }
    }
}
