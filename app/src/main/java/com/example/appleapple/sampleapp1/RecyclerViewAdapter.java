package com.example.appleapple.sampleapp1;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author Sooraj Soman on 4/2/2019
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolderClass> {
    private final Context context;
    private List<Itemclass> items;

    IUpdateEvent updateEvent;

    public RecyclerViewAdapter(List<Itemclass> items, Context context) {
        this.items = items;
        this.context = context;


    }

    public void setIUpateEvent(IUpdateEvent event) {
        this.updateEvent = event;
    }

    @Override
    public ViewHolderClass onCreateViewHolder(ViewGroup parent,
                                              int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);
        return new ViewHolderClass(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolderClass holder, final int position) {
        final Itemclass item = items.get(position);
        holder.set(item);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                items.remove(position);
                updateEvent.updateTotal(items);
                notifyDataSetChanged();

            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(item, position, holder);
                updateEvent.updateTotal(items);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public class ViewHolderClass extends RecyclerView.ViewHolder {
        TextView itemname, up, tgst, tsgst, tqty, Ttotal;
        ImageView delete, edit;

        public ViewHolderClass(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.Txtitem);
            up = (TextView) itemView.findViewById(R.id.Txt_up);
            tgst = (TextView) itemView.findViewById(R.id.Txt_gst);
            tsgst = (TextView) itemView.findViewById(R.id.Txt_sgst);
            tqty = (TextView) itemView.findViewById(R.id.TxtQty);
            Ttotal = (TextView) itemView.findViewById(R.id.Txttotal);
            delete = (ImageView) itemView.findViewById(R.id.can_order);
            edit = (ImageView) itemView.findViewById(R.id.btn_edit);

        }

        public void set(Itemclass item) {
            //UI setting code
            itemname.setText("Item  " + item.getItemname());
            up.setText("Unit Rate" + item.getUnitPrice());
            tqty.setText("Quantity" + item.getQty());
            Ttotal.setText("Total " + new DecimalFormat("#,###,###.##").format(Double.parseDouble(item.getTotal())));
            tgst.setText("Gst :" + item.getGst());
            tsgst.setText("Sgst  :" + item.getSgst());


        }
    }

    private void showDialog(final Itemclass currentItem, final int pos, final ViewHolderClass viewHolderClass) {
        final android.support.v7.app.AlertDialog.Builder mbuilder = new android.support.v7.app.AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        final View mview = inflater.inflate(R.layout.layout_edit_order, null);
        mbuilder.setView(mview);
        final android.support.v7.app.AlertDialog dialog = mbuilder.create();
        dialog.show();
        Button btndelete = (Button) mview.findViewById(R.id.btn_edit);
        final EditText editText1 = (EditText) mview.findViewById(R.id.tv_rate);
        final EditText editText2 = (EditText) mview.findViewById(R.id.tv_qty);
        editText1.setText(currentItem.getUnitPrice());
        editText2.setText(currentItem.getQty());
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editText1.getText().toString())) {
                    currentItem.setUnitPrice(editText1.getText().toString());

                }
                if (!TextUtils.isEmpty(editText2.getText().toString())) {
                    currentItem.setQty(editText2.getText().toString());
                }
                if (!TextUtils.isEmpty(currentItem.getUnitPrice()) && !TextUtils.isEmpty(currentItem.getQty())) {
                    Double qty = Double.parseDouble(currentItem.getQty());
                    int upr = Integer.parseInt(currentItem.getUnitPrice());
                    if (upr != 0) {
                        Double totVal = qty * upr;
                        double currentTotl = totVal + (totVal * (Integer.parseInt(currentItem.getGst()) * .01)) + (totVal * (Integer.parseInt(currentItem.getSgst()) * .01));
                        currentItem.setTotal(String.valueOf(currentTotl));


                    }
                    viewHolderClass.up.setText("Unit Rate " + currentItem.getUnitPrice());
                    viewHolderClass.tqty.setText("Quantity " + currentItem.getQty());
                    viewHolderClass.Ttotal.setText("Total " + new DecimalFormat("#,###,###.##").format(Double.parseDouble(currentItem.getTotal())));

                }
                items.set(pos, currentItem);
                updateEvent.updateTotal(items);
                notifyDataSetChanged();
                dialog.dismiss();

            }
            // String oid="1";


        });
        Button btncancel = (Button) mview.findViewById(R.id.can_order);
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
    }

    public List<Itemclass> getItems() {
        if (items != null && items.size() > 0) {
            return items;
        } else
            return null;
    }

    public interface IUpdateEvent {

        void updateTotal(List<Itemclass> items);

    }
}